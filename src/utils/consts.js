// Auth
export const BEARER = 'Bearer ';
export const TOKEN_COOKIE_KEY = 'TOKEN';

// User types
export const USER_TYPE = 'USER_TYPE';
export const SIMPLE_USER_TYPE = 'SIMPLE_USER_TYPE';

// Dialog types
export const LOGIN_DIALOG = 'LOGIN_DIALOG';
export const REGISTRATION_DIALOG = 'REGISTRATION_DIALOG';

// Panels
export const RES_PANEL_GREETING = 'RES_PANEL_GREETING';
export const RES_PANEL_USER = 'RES_PANEL_USER';
export const RES_PANEL_RESERVATIONS = 'RES_PANEL_RESERVATIONS';
export const RES_PANEL_RESERVATION_SUBMITTED = 'RES_PANEL_RESERVATION_SUBMITTED';
export const RES_PANEL_ORDER = 'RES_PANEL_ORDER';
export const RES_PANEL_ORDER_SUBMITTED = 'RES_PANEL_ORDER_SUBMITTED';

// UI
export const CURRENCY = ' £';

// Panel Button
export const NEXT = 'NEXT';
export const SUBMIT = 'SUBMIT';
export const ORDER = 'ORDER';
export const RESET = 'RESET';

// Restaurant
export const RESTAURANT_NAME = "Abraham & Russo";