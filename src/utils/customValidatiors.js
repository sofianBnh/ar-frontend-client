import {isEmail, isEmpty, isMobilePhone} from 'validator'

const success = 'success';
const error = 'error';


function prep(input) {
    input = input + '';
    input = input.trim();
    return input;
}

export function isValidName(input) {
    input = prep(input);
    if (min(input, 3) && max(input, 15))
        return success;

    return error
}

export function isValidEmail(input) {
    input = prep(input);
    if (isEmail(input))
        return success;

    return error
}

export function isValidPhone(input) {
    input = prep(input);
    if (isMobilePhone(input, "any"))
        return success;

    return error
}

export function isRequired(input) {
    input = prep(input);

    if (!isEmpty(input))
        return success;

    return error

}

export function isValidNumberOfPlaces(input) {
    if (!(input === "") && input > 0 && input < 15)
        return success;
    else
        return error
}

function _hoursBetween(start, end) {
    let date = '01/01/2018 ';

    let startTime = new Date(date + start.toString().trim());
    let endTime = new Date(date + end.toString().trim());
    if (startTime > endTime)
        return undefined;
    return (endTime - startTime) / (3600 * 1000);
}


export function validateSimpleUser({name, email, phone}) {

    if (isEmpty(name.toString().trim()) && !min(name, 3))
        throw new DOMException("Name field must be at least 3 characters long");

    if (!isMobilePhone(phone, "any"))
        throw new DOMException("Please enter a valid phone number");

    if (!isEmail(email))
        throw new DOMException("Please enter a valid email")
}

export function validateUser({name, email, phone, username, password}) {

    validateSimpleUser({name, email, phone});

    if (isEmpty(username.toString().trim()))
        throw new DOMException("Please enter a username");

    if (!min(password, 3))
        throw new DOMException("Password must be at least 3 characters long");

}

export function validateUserInput({userType, simpleUserId, userId}) {
    if (userId === '' && simpleUserId === '') {
        throw new DOMException("Login or Register to continue")
    }
}

export function validateReservation({restaurant, places, date, start, end}) {

    let hours = _hoursBetween(start, end);

    if (hours === undefined)
        throw new DOMException("Invalid times");

    if (!hours > 0 && hours < 3)
        throw new DOMException("Reservation too long");

    if (!(!(places === "") && places > 0 && places < 15))
        throw new DOMException("Invalid number of places");

    if (restaurant === undefined || restaurant === '')
        throw new DOMException("Restaurant not selected");

    let currentDate = new Date();
    currentDate = currentDate.getDate();

    if (!date || date === '')
        throw new DOMException("The date was not selected");

    if (new Date(date) < currentDate)
        throw new DOMException("Date selected prior to today");
}

export function validateOrder({meals, reservationId}) {
    if (!Array.isArray(meals) || meals.length === 0)
        throw new DOMException("Meals not selected");

    if (!reservationId || reservationId === '')
        throw new DOMException("Reservation Not selected")
}

export function validateLogin({username, password}) {
    if (isEmpty(username.toString().trim()))
        throw new DOMException("Username can't be empty");

    if (isEmpty(password.toString()))
        throw new DOMException("Password can't be empty");
}


function min(value, minSize) {
    return value.toString().length >= minSize
}

function max(value, maxSize) {
    return value.toString().length <= maxSize
}

