import _ from 'lodash'

export function initState(array, name) {
    name = name.toString();
    let result = {};
    let listAttributes = _.map(array, name);
    _.forEach(listAttributes, function (value) {
        result[value.toString().toLowerCase().trim()] = ''
    });
    return result;
}

export function genericHandleEvent(object, event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    object.setState({
        [name]: value,
    });
}

