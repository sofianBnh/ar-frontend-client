import {ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import React from "react";

export function createInputFiled(object, {name, type, options, validator, changeHandler}) {

    let extra = {};

    if (type === "select") {
        extra = {
            componentClass: "select"
        }
    }

    let lower = name.toString().toLowerCase();
    return <FormGroup
        controlId={lower}
        validationState={validator === undefined ? undefined : validator(object.state[lower])}

    >
        <ControlLabel>{name}</ControlLabel>

        <FormControl
            name={lower}
            type={type}
            value={object.state[lower]}
            placeholder={name + '...'}
            onChange={changeHandler}
            {...extra}
        >
            {options && options.map((option, index) => {
                return <option value={option.id} key={index}>{option.address}</option>
            })}

        </FormControl>
        <FormControl.Feedback/>
    </FormGroup>
}