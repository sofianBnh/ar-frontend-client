export function getError(error) {
    let errorText = error.response.data;

    if (errorText.message)
        errorText = errorText.message;
    return errorText;
}

