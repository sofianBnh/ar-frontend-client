import React from 'react';
import {Nav} from "react-bootstrap";
import HeaderUser from "./NavSides/HeaderUser";
import HeaderSettings from "./NavSides/HeaderSettings";
import HeaderLogin from "./NavSides/HeaderLogin";
import PropTypes from 'prop-types';

const HeaderRightSide = ({logged, username}) => {
    if (logged)
        return (
            <Nav pullRight>
                <HeaderUser username={username}/>
                <HeaderSettings/>
            </Nav>
        );

    else
        return (
            <HeaderLogin/>
        );
};


HeaderRightSide.propTypes = {
    logged: PropTypes.bool.isRequired,
    username: PropTypes.string,
};

export default HeaderRightSide;






