import React, {Component} from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import '../../styles/header.css'
import HeaderRightSide from "./HeaderRightSide";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {fetchToken} from "../../duck/actions/service/sessionActions";
import {LINKS} from '../Presentation/manifest'
import {RESTAURANT_NAME} from "../../utils/consts";


class Header extends Component {

    componentWillReceiveProps(nextProps) {
        if (nextProps.logged && nextProps.logged !== this.props.logged)
            this.props.fetchToken();
    }


    componentDidMount() {
        this.props.fetchToken();
    }


    render() {
        return (
            <Navbar collapseOnSelect fixedTop>
                <div className={"container"}>

                    <Navbar.Header>

                        <Navbar.Brand>
                            <a href="#hero" className={"logo"}>{RESTAURANT_NAME}</a>
                        </Navbar.Brand>

                        <Navbar.Toggle/>
                    </Navbar.Header>

                    <Navbar.Collapse>

                        <Nav>
                            {LINKS.map((link, index) => {
                                return <NavItem key={index} eventKey={index} href={link.link}>
                                    {link.title}
                                </NavItem>
                            })}
                        </Nav>

                        <HeaderRightSide logged={this.props.logged} username={this.props.username}/>

                    </Navbar.Collapse>

                </div>
            </Navbar>
        );
    }
}

const mapStateToProps = state => ({
    logged: state.session.logged,
    username: state.session.username,
});


Header.propTypes = {
    logged: PropTypes.bool.isRequired,
    username: PropTypes.string.isRequired,
    fetchToken: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {fetchToken})(Header);
