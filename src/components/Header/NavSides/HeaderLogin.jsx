import React, {Component} from 'react';
import {Nav, NavItem} from "react-bootstrap";
import {LOGIN_DIALOG, REGISTRATION_DIALOG} from "../../../utils/consts";
import {changeDialog, toggleDialog} from "../../../duck/actions/ui/dialogActions";
import {connect} from "react-redux";
import PropTypes from 'prop-types';

class HeaderLogin extends Component {

    handleSelect = (selectedKey) => {
        if (selectedKey === "login") {

            this.props.changeDialog(LOGIN_DIALOG);
            this.props.toggleDialog(true);

        } else if (selectedKey === "sign-up") {

            this.props.changeDialog(REGISTRATION_DIALOG);
            this.props.toggleDialog(true);

        }
    };


    render() {
        return (
            <Nav pullRight>
                <NavItem eventKey={"login"} href="#" onSelect={this.handleSelect}>
                    Login
                </NavItem>

                <NavItem eventKey={"sign-up"} href="#" onSelect={this.handleSelect}>
                    <span className={"sign-up"}>Sign Up</span>
                </NavItem>
            </Nav>
        );
    }
}

HeaderLogin.propTypes = {
    toggleDialog: PropTypes.func.isRequired,
    changeDialog: PropTypes.func.isRequired,
};

export default connect(null, {toggleDialog, changeDialog})(HeaderLogin);

