import React from 'react';
import {Navbar} from "react-bootstrap";
import PropTypes from 'prop-types';

const HeaderUser = ({username}) => {
    return (
        <Navbar.Text>
            <Navbar.Link href="#" className={"user"}>{username}</Navbar.Link>
        </Navbar.Text>
    );
};

HeaderUser.propTypes = {
    username: PropTypes.string.isRequired,
};

export default HeaderUser;
