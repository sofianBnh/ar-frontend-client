import React, {Component} from 'react';
import {Col, MenuItem, Nav, NavDropdown, NavItem} from "react-bootstrap";
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import {toggleUserReservations} from "../../../duck/actions/ui/userReservationsActions";
import {fetchReservations} from "../../../duck/actions/service/reservationActions";
import {logout} from "../../../duck/actions/service/sessionActions";

class HeaderSettings extends Component {

    handleSelect = (selectedKey) => {
        if (selectedKey === "logout") {
            this.logout();
        }
        else if (selectedKey === "user-reservations") {
            this.openPanel();
        }
    };

    logout = () => {
        this.props.logout();
    };

    openPanel = () => {
        this.props.toggleUserReservations(true);
        this.props.fetchReservations();
    };

    render() {
        return (

            <span>

                <Col lgHidden mdHidden smHidden className={"settings-small"}>
                <Nav>
                    <NavItem eventKey={"user-reservations"} onClick={this.openPanel}>Reservations</NavItem>
                    <NavItem eventKey={"logout"} onClick={this.logout}>Logout</NavItem>
                </Nav>
                </Col>

                <Col xsHidden className={"settings-big"}>
                    <Nav>
                        <NavDropdown
                            eventKey={"settings"}
                            title={<i className="fas fa-cog"/>}
                            id="basic-nav-dropdown"
                        >
                            <MenuItem eventKey={"user-reservations"} onSelect={this.handleSelect}>
                                Reservations
                            </MenuItem>

                            <MenuItem eventKey={"logout"} onSelect={this.handleSelect}>
                                Logout
                            </MenuItem>
                        </NavDropdown>
                    </Nav>
                </Col>
            </span>

        );
    }
}


export default connect(null, {toggleUserReservations, fetchReservations, logout})(HeaderSettings);

HeaderSettings.propTypes = {
    fetchReservations: PropTypes.func.isRequired,
    toggleUserReservations: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired
};