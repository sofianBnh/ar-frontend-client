import React, {Component} from 'react';
import {Alert, Button} from "react-bootstrap";
import {genericHandleEvent, initState} from "../../../utils/component-utils";
import {createInputFiled} from "../../../utils/inputs";
import {validateLogin} from "../../../utils/customValidatiors";
import PropTypes from 'prop-types';
import {fetchToken, login} from "../../../duck/actions/service/sessionActions";
import {connect} from "react-redux";
import {toggleDialog} from "../../../duck/actions/ui/dialogActions";

class LoginForm extends Component {


    login = () => {

        this.setState({error: ''});

        try {
            validateLogin(this.state)
        } catch (e) {
            this.setState({error: e.message});
            return;
        }

        this.props.login(this.state);

    };

    handleChange = (event) => {
        genericHandleEvent(this, event)
    };

    constructor(props) {
        super(props);

        this.inputs = [
            {
                name: 'Username',
                type: 'text',
                changeHandler: this.handleChange
            }, {
                name: 'Password',
                type: 'password',
                changeHandler: this.handleChange
            },
        ];

        this.state = {
            ...initState(this.inputs, 'name'),
            error: ''
        }

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.error && nextProps.error !== this.state.error) {
            this.setState({error: nextProps.error})
        }
    }

    render() {
        return (
            <div className={"login-form"}>

                {this.inputs.map((input, index) => {
                    return <div key={index}>{createInputFiled(this, input)}</div>
                })}

                {this.state.error && <Alert>{this.state.error}</Alert>}

                <Button block onClick={this.login}>LOGIN</Button>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    error: state.session.loginError,
});

LoginForm.propTypes = {
    error: PropTypes.string,
    login: PropTypes.func.isRequired,
    fetchToken: PropTypes.func.isRequired,
    toggleDialog: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {login, fetchToken, toggleDialog})(LoginForm);
