import React, {Component} from 'react';
import {isRequired, isValidEmail, isValidName, isValidPhone, validateUser} from "../../../utils/customValidatiors";
import {genericHandleEvent, initState} from "../../../utils/component-utils";
import {createInputFiled} from "../../../utils/inputs";
import {Alert, Button, Row} from "react-bootstrap";
import PropTypes from 'prop-types';
import {registerUser} from "../../../duck/actions/service/registrationActions";
import {connect} from "react-redux";
import {toggleDialog} from "../../../duck/actions/ui/dialogActions";
import Col from "react-bootstrap/es/Col";

class RegisterForm extends Component {

    submit = () => {

        this.setState({error: ''});

        try {
            validateUser(this.state)
        } catch (e) {
            this.setState({error: e.message});
            return;
        }

        this.props.registerUser(this.state);
        // todo add some message
    };

    handleChange = (event) => {
        genericHandleEvent(this, event)
    };

    constructor(props) {
        super(props);

        this.inputs = [
            {
                name: "Name",
                type: "text",
                validator: isValidName,
                changeHandler: this.handleChange,
                help: 'Your full name'

            }, {
                name: "Email",
                type: "email",
                validator: isValidEmail,
                changeHandler: this.handleChange,
                help: 'Your email'
            }, {
                name: "Phone",
                type: "phone",
                validator: isValidPhone,
                changeHandler: this.handleChange,
                help: 'Your phone number'

            },
        ];

        this.loginInputs = [
            {
                name: "Username",
                type: "text",
                validator: isRequired,
                changeHandler: this.handleChange,
                help: 'Your username'
            }, {
                name: "Password",
                type: "password",
                validator: isRequired,
                changeHandler: this.handleChange,
                help: 'Your password'
            },
        ];

        this.state = {
            ...initState(this.inputs, 'name'),
            ...initState(this.loginInputs, 'name'),
        }

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.error && nextProps.error !== this.state.error) {
            this.setState({error: nextProps.error})
        }
    }

    render() {
        return (
            <div className={"registration-form"}>


                <Row>
                    <Col lg={6} md={6}>
                        <div className="dialog-form">
                            <h3>User Info</h3>
                            <hr/>

                            {this.inputs.map((input, index) => {
                                return <div key={index}>{createInputFiled(this, input)}</div>
                            })}
                        </div>

                    </Col>

                    <Col lg={6} md={6}>
                        <div className="dialog-form">
                            <h3>Login Info</h3>
                            <hr/>

                            {this.loginInputs.map((input, index) => {
                                return <div key={index}>{createInputFiled(this, input)}</div>
                            })}
                        </div>
                    </Col>
                </Row>

                {this.state.error && <Alert>{this.state.error}</Alert>}

                <div className={"registration-button"}>
                    <Button block onClick={this.submit}>SUBMIT</Button>
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    error: state.registration.userRegistrationError,
});

RegisterForm.propTypes = {
    error: PropTypes.string,
    registerUser: PropTypes.func.isRequired,
    toggleDialog: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {registerUser, toggleDialog})(RegisterForm);
