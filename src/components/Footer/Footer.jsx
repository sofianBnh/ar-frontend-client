import React from 'react';
import {Col, Row} from "react-bootstrap";
import '../../styles/footer.css'
import {RESTAURANT_NAME} from "../../utils/consts";

const content = {

    service: {
        title: "Services",
        services: [
            {title: "Breakfast", link: "#"},
            {title: "Lunch", link: "#"},
            {title: "Dinner", link: "#"}
        ]
    },

    contact: {
        title: "Contact",
        contacts: [
            {title: "Phone", link: '#'},
            {title: "Email", link: '#'},
            {title: "Find Us", link: '#'}
        ]
    },


    company: {
        title: RESTAURANT_NAME,
        description: "Praesent sed lobortis mi. Suspendisse vel " +
        "placerat ligula. Vivamus ac sem lacus.\nUt vehicula rhoncus " +
        "elementum. Etiam quis tristique lectus. Aliquam in arcu eget\n" +
        "elementum. Etiam quis tristique lectus. Aliquam in arcu eget\n" +
        "velit pulvinar dictum vel in justo."
    },

    links: [
        {faRef: "fab fa-facebook-f", link: "#"},
        {faRef: "fab fa-twitter", link: "#"},
        {faRef: "fab fa-instagram", link: "#"},
        {faRef: "fab fa-pinterest-p", link: "#"}
    ]

};

const Footer = () => {

    function about() {
        return <Col md={6} mdPush={6} className="item text">
            <h3>{content.company.title}</h3>
            <p>{content.company.description}</p>
        </Col>;
    }

    function services() {
        return <Col md={3} mdPull={6} sm={4} xs={6} className="item text">
            <h3>{content.service.title}</h3>

            <ul>
                {content.service.services.map((service, index) => {
                    return <li key={index}><a href={service.link}>{service.title}</a></li>
                })}
            </ul>
        </Col>;
    }

    function contacts() {
        return <Col md={3} mdPull={6} sm={4} xs={6} className="item text">
            <h3>{content.contact.title}</h3>

            <ul>
                {content.contact.contacts.map((contact, index) => {
                    return <li key={index}><a href={contact.link}>{contact.title}</a></li>
                })}
            </ul>
        </Col>;
    }

    function links() {
        return <Col md={12} sm={4} className="item social">

            {content.links.map((link, index) => {
                return <a key={index} href={link.link}>
                    <i className={link.faRef}/>
                </a>
            })}

        </Col>;
    }

    return (

        <div className="footer-dark">
            <footer>

                <div className="container">
                    <Row>

                        <Col lgHidden mdHidden>
                            <hr/>
                        </Col>

                        {about()}

                        <Col lgHidden mdHidden>
                            <hr/>
                        </Col>


                        {services()}

                        {contacts()}

                        {links()}

                    </Row>

                    <p className="copyright">{content.company.title} © 2018</p>

                </div>
            </footer>
        </div>
    );
};

export default Footer;
