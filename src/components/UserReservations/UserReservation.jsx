import React, {Component} from 'react';
import {Col, Row} from "react-bootstrap";
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {deleteReservation, fetchReservations, updateReservation} from "../../duck/actions/service/reservationActions";
import {CURRENCY} from "../../utils/consts";
import _ from "lodash";
import UserReservationEdit from "./UserReservationEdit";
import {fetchRestaurants} from "../../duck/actions/resources/restaurantActions";
import {openUserReservation} from "../../duck/actions/ui/userReservationsActions";
import moment from 'moment';

const time = (time) => moment(time).format('HH:mm');
const day = (date) => moment(date).format('MMMM Do YYYY');

class UserReservation extends Component {

    static getTableIds = (tables) => {
        let idsList = _.map(tables, "id");
        let result = "";
        _.forEach(idsList, id => {
            result += id + ", "
        });
        return result.substring(0, result.length - 2)
    };

    onDelete = () => {
        this.props.deleteReservation(this.props.data.id);
    };

    onUpdate = (reservation, old) => {
        this.props.updateReservation({
            ...reservation,
            userId: this.props.userId,
        }, old)
    };

    constructor(props) {
        super(props);
        this.state = {};
    }


    static displayStatus(confirmed) {
        if (confirmed)
            return <span>
                <i className="fas fa-check-circle"/>
                <span className={"res-status"}>{"Confirmed"}</span>
            </span>;
        else
            return <span>
                    <i className="fas fa-times-circle"/>
                    <span className={"res-status"}>{"Waiting for confirmation"}</span>
                </span>;
    }

    componentDidMount() {
        if (this.props.restaurants || this.props.restaurants === []) {
            this.props.fetchRestaurants();
        }
    }

    render() {
        return (
            <div className={"reservation-slide"} key={this.index}>
                <Row>
                    <Col lg={4} md={4}>
                        <div className={"res-head"}>
                            <h3>{this.props.data.restaurant.address}</h3>

                            <p>
                                <b>{UserReservation.displayStatus(this.props.data.confirmed)}</b>
                            </p>

                            <div className={"hidden-md hidden-lg sep"}>
                                <hr/>
                            </div>
                        </div>
                    </Col>

                    <Col lg={4} md={4} sm={6}>
                        <div className={"res-data"}>
                            <p><b> <i className="far fa-calendar"/> Date</b> : {day(this.props.data.date)}</p>

                            <p><b><i className="far fa-clock"/> Starting Time</b> : {time(this.props.data.startTime)}
                            </p>

                            <p><b><i className="fas fa-clock"/> Ending Time</b> : {time(this.props.data.endTime)}</p>

                        </div>
                    </Col>

                    <Col lg={4} md={4} sm={6}>
                        <div className={"res-data"}>
                            <p><b><i
                                className="fas fa-couch"/> Table(s)</b>
                                : {UserReservation.getTableIds(this.props.data.tables)}
                            </p>
                            {this.props.data.price &&
                            <p><b><i className="fas fa-shopping-cart"/> Cost</b> : {this.props.data.price + CURRENCY}
                            </p>}
                        </div>
                    </Col>


                </Row>


                <Row>
                    <Col>
                        <div className={"res-action"}>
                            <UserReservationEdit onDelete={this.onDelete}
                                                 onUpdate={this.onUpdate}
                                                 openReservation={this.props.openReservation}
                                                 openUserReservation={this.props.openUserReservation}
                                                 old={this.props.data}
                                                 restaurants={this.props.restaurants}
                            />
                        </div>

                    </Col>
                </Row>
            </div>
        );
    }
}


UserReservation.propTypes = {
    data: PropTypes.shape({
        id: PropTypes.number.isRequired,
        confirmed: PropTypes.bool.isRequired,
        restaurant: PropTypes.shape({
            address: PropTypes.string.isRequired,
        }),
        price: PropTypes.number,
        date: PropTypes.string,
        startTime: PropTypes.string.isRequired,
        endTime: PropTypes.string.isRequired,
        tables: PropTypes.array.isRequired,
    }),
    deleteReservation: PropTypes.func.isRequired,
    fetchReservations: PropTypes.func.isRequired,
    updateReservation: PropTypes.func.isRequired,
    fetchRestaurants: PropTypes.func.isRequired,
    openReservation: PropTypes.number.isRequired,
    openUserReservation: PropTypes.func.isRequired,
    userId: PropTypes.number.isRequired,
    restaurants: PropTypes.array,
};

const mapStateToProps = state => ({
    openReservation: state.userReservations.openReservation,
    restaurants: state.restaurant.restaurants,
});

export default connect(mapStateToProps, {
    deleteReservation,
    fetchReservations,
    fetchRestaurants,
    openUserReservation,
    updateReservation,
})(UserReservation);
