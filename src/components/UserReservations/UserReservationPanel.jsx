import React, {Component} from 'react';
import {Button, Fade} from "react-bootstrap";
import '../../styles/user-reservations.css';
import {toggleUserReservations} from "../../duck/actions/ui/userReservationsActions";
import {fetchReservations} from "../../duck/actions/service/reservationActions";
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import ReservationsList from "./ReservationList";

class UserReservationPanel extends Component {


    closePanel = () => {
        this.setState({error: ''});
        this.props.toggleUserReservations(false)
    };

    constructor(props) {
        super(props);
        this.state = {
            reservations: [],
            error: ''
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({error: ''});
        if (nextProps.error && nextProps.error !== this.state.error) {
            this.setState({error: nextProps.error})
        }
        if (nextProps.delError && nextProps.delError !== this.state.delError) {
            this.setState({error: nextProps.delError})
        }
    }

    componentDidMount() {
        this.props.fetchReservations();
        this.setState({error: ''});
    }

    render() {
        return (
            <div>
                {this.props.open && <Fade in={!this.props.in}>
                    {this.reservations()}
                </Fade>}
            </div>

        );
    }

    reservations() {
        return <div className={"user-reservations-panel"}>
            <div className={"container inside-user-panel"}>
                <div className={"user-data"}>

                    <Button type="button" className="close" aria-label="Close"
                            onClick={this.closePanel}>
                        <i className="far fa-times-circle"/>
                    </Button>

                </div>

                <h2 className={"text-center"}>Reservations</h2>

                {<ReservationsList reservations={this.props.reservations} error={this.state.error}
                                   userId={this.props.userId}/>}

            </div>
        </div>;
    }
}

UserReservationPanel.propTypes = {
    toggleUserReservations: PropTypes.func.isRequired,
    fetchReservations: PropTypes.func.isRequired,
    reservations: PropTypes.array.isRequired,
    open: PropTypes.bool.isRequired,
    error: PropTypes.string,
    delError: PropTypes.string,
    userId: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({
    reservations: state.reservation.reservations,
    error: state.reservation.reservationsError,
    open: state.userReservations.userReservationsOpen,
    delError: state.reservation.reservationDeleteError,
    userId: state.reservationOwner.userId,
});

export default connect(mapStateToProps, {
    fetchReservations,
    toggleUserReservations,
})(UserReservationPanel);