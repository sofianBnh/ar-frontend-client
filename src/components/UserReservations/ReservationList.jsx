import React from 'react';
import PropTypes from 'prop-types';
import UserReservation from "./UserReservation";
import {Alert} from "react-bootstrap";

const ReservationsList = ({reservations, error, userId}) => {

    function displayList() {
        if (reservations) {
            if (reservations.length > 0) {
                return <div>
                    <div className={"reservations-lead text-center"}>
                        <h3><b>List of reservations you made</b></h3>
                        {error && <Alert>{error}</Alert>}
                    </div>

                    <div className="res-list">
                        {reservations.map((reservation, index) => {
                            return <div key={index}>
                                <UserReservation data={reservation} userId={userId}/>
                            </div>
                        })}
                    </div>
                </div>
            }

            return <div className={"reservations-lead text-center"}>
                <h3><b>You did't make reservations yet</b></h3>
            </div>
        }

        return <div className={"reservations-lead text-center"}>
            <h3><b>An error occurred</b></h3>
        </div>
    }

    return (
        <div>
            {displayList()}
        </div>
    );
};

ReservationsList.propTypes = {
    reservations: PropTypes.array,
    userId: PropTypes.number.isRequired,
    err: PropTypes.string,
};


export default ReservationsList;
