import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Alert, Col, Collapse, Row} from "react-bootstrap";
import {genericHandleEvent, initState} from "../../utils/component-utils";
import {isValidNumberOfPlaces, validateReservation} from "../../utils/customValidatiors";
import {createInputFiled} from "../../utils/inputs";
import _ from "lodash";
import {PacmanLoader} from "react-spinners";
import moment from "moment";


class UserReservationEdit extends Component {

    onEdit = () => {
        this.props.openUserReservation(this.props.old.id);
        this.setState({open: !this.state.open});
    };

    onUpdate = () => {

        if (!this.state.open)
            return;

        this.setState({error: ''});
        // validate
        try {
            validateReservation(this.state);
        } catch (e) {
            this.setState({error: e.message});
            return;
        }

        this.props.onUpdate(this.state, this.props.old)
    };

    onDelete = () => {
        this.props.onDelete(this.props.old.id)
    };

    handleChange = (event) => {
        genericHandleEvent(this, event)
    };

    //===================================================================

    showRestaurantFields = () => {
        if (this.props.restaurants && this.props.restaurants.length > 0) {

            this.filedInputs[0].options = this.props.restaurants;

            let list = [];
            _.forEach(this.filedInputs, (input, index) => {
                list.push(<div key={index}>{createInputFiled(this, input)}</div>)
            });

            return list;
        }

        return <div className={"res-panel-spinner"}>
            <div className={"text-center"}><PacmanLoader color={'#343434'}/></div>
        </div>
    };

    //===================================================================

    loadEdit = (nextProps) => {
        const dateString = moment(nextProps.old.date).format('YYYY-MM-DD');
        const temp = {
            end: nextProps.old.endTime,
            start: nextProps.old.startTime,
            restaurant: nextProps.old.restaurant.id,
            date: dateString,
        };

        this.setState({
            ...this.state,
            ...temp,
        })
    };

    //===================================================================

    constructor(props, context) {
        super(props, context);

        this.dateInputs = [
            {
                name: "Date",
                type: "date",
                help: "Date for the reservation",
                changeHandler: this.handleChange
            }, {
                name: "Start",
                type: "time",
                help: "Start time of the reservation",
                changeHandler: this.handleChange
            }, {
                name: "End",
                type: "time",
                help: "End time of the reservation",
                changeHandler: this.handleChange
            },
        ];

        this.filedInputs = [
            {
                name: "Restaurant",
                type: "select",
                validator: undefined,
                options: this.props.restaurants,
                changeHandler: this.handleChange
            }, {
                name: "Places",
                type: "number",
                validator: isValidNumberOfPlaces,
                changeHandler: this.handleChange
            },
        ];

        this.state = {
            ...initState(this.filedInputs, 'name'),
            ...initState(this.dateInputs, 'name'),
            open: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.old && nextProps.old !== this.props.old) {
            this.loadEdit(nextProps);
        }

        if (nextProps.openReservation && nextProps.openReservation !== this.props.openReservation) {
            if (nextProps.openReservation !== this.props.old.id) {
                this.setState({open: false})
            }
        }
    }


    componentDidMount() {
        if (this.props.old) {
            this.loadEdit(this.props);
        }
    }


    //===================================================================

    render() {
        return (
            <div>

                <div className={"user-res-buttons"}>
                    <hr/>

                    <div className={"res-user-update"}>
                        <button style={{marginLeft: 0}} onClick={this.onUpdate}>
                            <i className={'far fa-save'} style={{marginRight: '0.5em'}}/> Update
                        </button>

                    </div>

                    <div style={{textAlign: 'right'}}>

                        <button onClick={this.onEdit}><i
                            className={'far fa-edit'}
                            style={{marginRight: '0.5em'}}
                        /> Edit
                        </button>

                        <button onClick={this.onDelete}>
                            <i className={'far fa-trash-alt'} style={{marginRight: '0.5em'}}/> Delete
                        </button>

                    </div>

                </div>

                <Collapse in={this.state.open}>

                    <div>
                        <hr/>
                        <Row>
                            <Col lg={6} md={6}>
                                {this.showRestaurantFields()}
                            </Col>

                            <Col lg={6} md={6}>
                                {this.dateInputs.map((input, index) => {
                                    return <div key={index}>{createInputFiled(this, input)}</div>
                                })}
                            </Col>
                        </Row>

                        {this.state.error && <Alert>{this.state.error}</Alert>}
                    </div>
                </Collapse>
            </div>
        );
    }
}

UserReservationEdit.propTypes = {
    onDelete: PropTypes.func.isRequired,
    onUpdate: PropTypes.func.isRequired,
    openReservation: PropTypes.number.isRequired,
    openUserReservation: PropTypes.func.isRequired,
    restaurants: PropTypes.array,
    old: PropTypes.object.isRequired,
};

export default UserReservationEdit;
