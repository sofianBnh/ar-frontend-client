import React from "react";
import ReservationGreeting from "./panels/ReservationGreeting";
import UserInput from "./panels/reservation/ReservationUserInput";
import ReservationForm from "./panels/reservation/ReservationRestaurant";
import {
    RES_PANEL_GREETING,
    RES_PANEL_ORDER,
    RES_PANEL_ORDER_SUBMITTED,
    RES_PANEL_RESERVATION_SUBMITTED,
    RES_PANEL_RESERVATIONS,
    RES_PANEL_USER
} from "../../utils/consts";
import ReservationSubmitted from "./panels/reservation/ReservationSubmitted";
import OrderMain from "./panels/order/OrderMain";
import OrderSubmitted from "./panels/order/OrderSubmitted";


const panels = [
    RES_PANEL_GREETING,
    RES_PANEL_USER,
    RES_PANEL_RESERVATIONS,
    RES_PANEL_RESERVATION_SUBMITTED,
    RES_PANEL_ORDER,
    RES_PANEL_ORDER_SUBMITTED,
];

export function getPanelByName(panelName) {
    switch (panelName) {

        case RES_PANEL_GREETING:
            return <ReservationGreeting/>;

        case RES_PANEL_USER:
            return <UserInput/>;

        case RES_PANEL_RESERVATIONS:
            return <ReservationForm/>;

        case RES_PANEL_RESERVATION_SUBMITTED:
            return <ReservationSubmitted/>;

        case RES_PANEL_ORDER:
            return <OrderMain/>;

        case RES_PANEL_ORDER_SUBMITTED:
            return <OrderSubmitted/>;

        default:
            return null;
    }
}


export function getNextPanel(panelName) {
    let index = panels.indexOf(panelName);
    return panels[(index + 1) % panels.length];
}