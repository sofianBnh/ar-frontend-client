import React from 'react';
import {Button, Col, Row} from "react-bootstrap";
import PropTypes from 'prop-types';

const ReservationBar = ({title, next}) => {


    return (
        <div className={"bar"}>
            <Row>
                <Col lg={10} md={10} smHidden xsHidden>
                </Col>
                <Col lg={2} md={2}>
                    <Button className={"next"} block onClick={next}>
                        {title}
                    </Button>
                </Col>
            </Row>
        </div>
    );
};


ReservationBar.propTypes = {
    title: PropTypes.string.isRequired,
    next: PropTypes.func.isRequired,
};

export default ReservationBar;
