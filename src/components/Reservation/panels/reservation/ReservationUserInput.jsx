import React, {Component} from 'react';
import {Tab, Tabs} from "react-bootstrap";
import LoginForm from "../../../common/forms/LoginForm";
import ClientForm from "./forms/ClientForm";
import {RES_PANEL_USER, SIMPLE_USER_TYPE, USER_TYPE} from "../../../../utils/consts";
import {validateUserInput} from "../../../../utils/customValidatiors";
import {connect} from "react-redux";
import {setUserTypeReservationService} from "../../../../duck/actions/service/reservationOwnerActions";
import PropTypes from 'prop-types';
import {changePanelContent, changePanelError, deactivateTrigger} from "../../../../duck/actions/ui/panelActions";
import {getNextPanel} from "../../manifest";


const currentPanel = RES_PANEL_USER;

class ReservationUserInput extends Component {

    trigger = () => {
        this.props.changePanelError("");

        if (this.props.userType === SIMPLE_USER_TYPE) {
            this.child.send();
        }

        if (this.props.userType === USER_TYPE) {

            if (!this.props.logged) {
                this.props.changePanelError("You have to login or register");
            }
        }
        this.props.deactivateTrigger(currentPanel);

    };

    toggleNextPanel = () => {

        this.props.changePanelError('');

        try {
            validateUserInput({
                userType: this.props.userType,
                userId: this.props.userId,
                simpleUserId: this.props.simpleUserId,
            })
        } catch (e) {
            this.props.changePanelError(e.message);
            this.props.deactivateTrigger(currentPanel);
            return;
        }

        this.props.changePanelContent(getNextPanel(currentPanel));

    };

    handleSelect = (type) => {
        this.props.setUserTypeReservationService(type);
    };

    displayLogin = () => {
        if (!this.props.logged) {
            return <LoginForm/>
        } else {
            return <div className={"reservation-text"}>
                <p>Continue as <b>{this.props.username}</b></p>
            </div>
        }
    };

    displayClientForm = () => {
        if (this.props.simpleUserId === '') {
            return <ClientForm onRef={ref => (this.child = ref)}/>
        } else {
            return <div className={"reservation-text"}>
                <p>Your credentials have been submitted</p>
            </div>
        }
    };

    constructor(props) {
        super(props);
        this.child = React.createRef()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.trigger && nextProps.trigger !== this.props.trigger) {
            this.trigger();
        }

        if (nextProps.registered && nextProps.registered !== this.props.registered) {
            this.toggleNextPanel();
        }

        if (nextProps.fetchedId) {
            this.toggleNextPanel();
        }

    }

    render() {
        return (
            <Tabs
                activeKey={this.props.userType}
                onSelect={this.handleSelect}
                id="user-input-tab"
            >

                <Tab eventKey={USER_TYPE} title="USER" disabled={this.props.registered}>
                    <div className={"reservation-input"}>
                        {!this.props.registered && this.displayLogin()}
                    </div>
                </Tab>

                <Tab eventKey={SIMPLE_USER_TYPE} title="SIMPLE USER" disabled={this.props.logged}>
                    <div className={"reservation-input"}>
                        {!this.props.logged && this.displayClientForm()}
                    </div>
                </Tab>

            </Tabs>
        );

    }
}

ReservationUserInput.propTypes = {
    username: PropTypes.any,
    userId: PropTypes.any,
    simpleUserId: PropTypes.any,
    userType: PropTypes.string,

    trigger: PropTypes.bool.isRequired,
    registered: PropTypes.bool.isRequired,
    logged: PropTypes.bool.isRequired,
    fetchedId: PropTypes.bool.isRequired,

    setUserTypeReservationService: PropTypes.func.isRequired,
    changePanelError: PropTypes.func.isRequired,
    changePanelContent: PropTypes.func.isRequired,
    deactivateTrigger: PropTypes.func.isRequired,

};

const mapStateToProps = state => ({
    username: state.session.username,
    userId: state.session.userId,
    logged: state.session.logged,
    fetchedId: state.session.fetchedId,

    registered: state.registration.simpleUserRegistered,
    simpleUserId: state.registration.simpleUserId,

    trigger: state.panel.triggerUser,
    userType: state.reservationOwner.userType,
});

export default connect(mapStateToProps, {
    setUserTypeReservationService,
    changePanelError,
    changePanelContent,
    deactivateTrigger,
})(ReservationUserInput);
