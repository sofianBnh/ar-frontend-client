import React, {Component} from 'react';
import {isValidEmail, isValidName, isValidPhone, validateSimpleUser} from "../../../../../utils/customValidatiors";
import {genericHandleEvent, initState} from "../../../../../utils/component-utils";
import {createInputFiled} from "../../../../../utils/inputs";
import {connect} from "react-redux";
import {registerSimpleUser} from "../../../../../duck/actions/service/registrationActions";
import {changePanelError} from "../../../../../duck/actions/ui/panelActions";
import PropTypes from 'prop-types';
import {Alert} from "react-bootstrap";

class ClientForm extends Component {


    send = () => {
        this.props.changePanelError('');

        try {
            validateSimpleUser(this.state)
        } catch (e) {
            this.props.changePanelError(e.message);
            return;
        }

        this.props.registerSimpleUser(this.state);
    };

    handleChange = (event) => {
        genericHandleEvent(this, event)
    };

    constructor(props) {
        super(props);

        this.inputs = [
            {
                name: "Name",
                type: "text",
                validator: isValidName,
                changeHandler: this.handleChange
            },
            {
                name: "Email",
                type: "email",
                validator: isValidEmail,
                changeHandler: this.handleChange
            },
            {
                name: "Phone",
                type: "phone",
                validator: isValidPhone,
                changeHandler: this.handleChange
            },
        ];

        this.state = {
            ...initState(this.inputs, 'name')
        }
    }

    componentDidMount() {
        this.props.onRef(this)
    }

    componentWillUnmount() {
        this.props.onRef(undefined)
    }

    render() {
        return (
            <div className={"client-form"}>
                {this.inputs.map((input, index) => {
                    return <div key={index}>{createInputFiled(this, input)}</div>
                })}

                {this.props.error && <Alert>{this.props.error}</Alert>}

            </div>
        );
    }
}

ClientForm.propTypes = {
    error: PropTypes.string,

    trigger: PropTypes.bool,
    registerSimpleUser: PropTypes.func.isRequired,
    changePanelError: PropTypes.func.isRequired,

    onRef: PropTypes.func.isRequired,
};


const mapStateToProps = state => ({
    error: state.registration.simpleUserRegistrationError,
});

export default connect(mapStateToProps, {registerSimpleUser, changePanelError})(ClientForm);
