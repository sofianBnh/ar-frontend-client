import React, {Component} from 'react';
import {getNextPanel} from "../../manifest";
import {ORDER, RES_PANEL_GREETING, RES_PANEL_RESERVATION_SUBMITTED, RESET} from "../../../../utils/consts";
import {changeButtonTitle, changePanelContent, resetPanel} from "../../../../duck/actions/ui/panelActions";
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import {resetReservation} from "../../../../duck/actions/service/reservationActions";
import {Button} from "react-bootstrap";
import _ from 'lodash';
import {resetRegistration} from "../../../../duck/actions/service/registrationActions";

const currentPanel = RES_PANEL_RESERVATION_SUBMITTED;

const text = "Your reservation has been submitted, Your table(s) : ";

class ReservationSubmitted extends Component {

    clear = () => {
        this.props.resetPanel();
        this.props.resetReservation();
        this.props.resetRegistration();
        this.props.changePanelContent(RES_PANEL_GREETING);
    };

    trigger = () => {
        if (this.props.logged)
            this.props.changePanelContent(getNextPanel(currentPanel));
        else
            this.clear();
    };

    getTableIds = () => {
        let idsList = _.map(this.props.reservation.tables, "id");
        let result = "";
        _.forEach(idsList, id => {
            result += id + ", "
        });
        return result.substring(0, result.length - 2)
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.trigger)
            this.trigger();
    }

    componentDidMount() {
        if (this.props.logged)
            this.props.changeButtonTitle(ORDER);
        else
            this.props.changeButtonTitle(RESET);
    }

    render() {
        let extras;
        if (this.props.logged) {
            extras = <div>
                <p>Would you like to add an Order ? Press the Order button</p>
                <Button onClick={this.clear}>No thank you</Button>
            </div>
        }

        return (
            <div className={"reservation-text"}>
                <p>
                    {text + this.getTableIds()}
                </p>

                {extras && extras}
            </div>
        );
    }
}

ReservationSubmitted.propTypes = {
    trigger: PropTypes.bool.isRequired,
    logged: PropTypes.bool.isRequired,
    reservation: PropTypes.object.isRequired,

    resetPanel: PropTypes.func.isRequired,
    resetRegistration: PropTypes.func.isRequired,
    resetReservation: PropTypes.func.isRequired,
    changePanelContent: PropTypes.func.isRequired,
    changeButtonTitle: PropTypes.func.isRequired,

};


const mapStateToProps = state => ({
    logged: state.session.logged,
    trigger: state.panel.triggerSubmit,
    reservation: state.reservation.reservation,
});

export default connect(mapStateToProps, {
    resetPanel,
    resetReservation,
    resetRegistration,
    changePanelContent,
    changeButtonTitle
})(ReservationSubmitted);
