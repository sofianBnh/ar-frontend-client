import React, {Component} from 'react';
import {Col, Row} from "react-bootstrap";
import _ from 'lodash';
import {isValidNumberOfPlaces, validateReservation} from "../../../../utils/customValidatiors";
import {genericHandleEvent, initState} from "../../../../utils/component-utils";
import {createInputFiled} from "../../../../utils/inputs";
import {fetchRestaurants} from "../../../../duck/actions/resources/restaurantActions";
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import {PacmanLoader} from "react-spinners";
import {RES_PANEL_RESERVATIONS} from "../../../../utils/consts";
import {getNextPanel} from "../../manifest";
import {changePanelContent, changePanelError, deactivateTrigger} from "../../../../duck/actions/ui/panelActions";
import {submitReservation} from "../../../../duck/actions/service/reservationActions";

const currentPanel = RES_PANEL_RESERVATIONS;

class ReservationRestaurant extends Component {


    trigger = () => {
        this.props.changePanelError('');

        if (!this.state.restaurant || this.state.restaurant === '') {
            this.setState({restaurant: this.props.restaurants[0].id}, () => {
                this.executeTrigger();
            });
        } else {
            this.executeTrigger();
        }

    };

    executeTrigger = () => {
        try {
            validateReservation(this.state);
        } catch (e) {
            this.props.deactivateTrigger(currentPanel);
            this.props.changePanelError(e.message);
            return;
        }

        this.props.submitReservation({
            ...this.props.reservation,
            ...this.state,
        });
    };

    handleChange = (event) => {
        genericHandleEvent(this, event)
    };

    showFields = () => {
        if (this.props.restaurants && this.props.restaurants.length > 0) {

            this.filedInputs[0].options = this.props.restaurants;

            let list = [];
            _.forEach(this.filedInputs, (input, index) => {
                list.push(<div key={index}>{createInputFiled(this, input)}</div>)
            });

            return list;
        }

        return <div className={"res-panel-spinner"}>
            <div className={"text-center"}><PacmanLoader color={'#222'}/></div>
        </div>
    };

    constructor(props) {

        super(props);

        this.dateInputs = [
            {
                name: "Date",
                type: "date",
                help: "Date for the reservation",
                changeHandler: this.handleChange
            }, {
                name: "Start",
                type: "time",
                help: "Start time of the reservation",
                changeHandler: this.handleChange
            }, {
                name: "End",
                type: "time",
                help: "End time of the reservation",
                changeHandler: this.handleChange
            },
        ];

        this.filedInputs = [
            {
                name: "Restaurant",
                type: "select",
                validator: undefined,
                options: this.props.restaurants,
                changeHandler: this.handleChange
            }, {
                name: "Places",
                type: "number",
                validator: isValidNumberOfPlaces,
                changeHandler: this.handleChange
            },
        ];

        this.state = {
            ...initState(this.filedInputs, 'name'),
            ...initState(this.dateInputs, 'name'),
        };

    }

    componentDidMount() {
        this.props.fetchRestaurants();
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.trigger && nextProps.trigger !== this.props.trigger) {
            this.trigger();
        }

        if (nextProps.submitted) {
            this.props.changePanelContent(getNextPanel(currentPanel));
        } else {
            this.props.deactivateTrigger(currentPanel);
            if (this.props.reservationsError) {
                this.props.changePanelError(this.props.reservationsError);
            }
        }

    }

    render() {
        return (
            <div className={"restaurant-form"}>

                <Row>
                    <Col lg={6} md={6}>
                        <h3>Restaurant</h3>
                        <hr/>

                        {this.showFields()}
                    </Col>

                    <Col lg={6} md={6}>
                        <h3>Time</h3>
                        <hr/>

                        {this.dateInputs.map((input, index) => {
                            return <div key={index}>{createInputFiled(this, input)}</div>
                        })}
                    </Col>
                </Row>

                <hr/>

            </div>
        );
    }
}

ReservationRestaurant.propTypes = {
    trigger: PropTypes.bool.isRequired,
    reservationsError: PropTypes.string,
    restaurants: PropTypes.array.isRequired,
    reservation: PropTypes.object,

    changePanelError: PropTypes.func.isRequired,
    changePanelContent: PropTypes.func.isRequired,
    fetchRestaurants: PropTypes.func.isRequired,
    submitReservation: PropTypes.func.isRequired,
    deactivateTrigger: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    submitted: state.reservation.submitted,
    trigger: state.panel.triggerReservation,
    restaurants: state.restaurant.restaurants,
    reservationError: state.reservation.reservationError,
    reservation: state.reservationOwner,
});

export default connect(mapStateToProps, {
    fetchRestaurants,
    changePanelError,
    submitReservation,
    deactivateTrigger,
    changePanelContent,
})(ReservationRestaurant);
