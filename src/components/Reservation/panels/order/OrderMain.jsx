import React, {Component} from 'react';
import {connect} from "react-redux";
import {Alert, Col, Row} from "react-bootstrap";
import _ from "lodash";
import MealTypes from "./sub-panels/MealTypes";
import MealsList from "./sub-panels/MealsList";
import ClientOrders from "./sub-panels/ClientOrders";
import PropTypes from 'prop-types';
import {fetchMeals} from "../../../../duck/actions/resources/mealActions";
import {RES_PANEL_ORDER} from "../../../../utils/consts";
import {validateOrder} from "../../../../utils/customValidatiors";
import {changePanelContent, changePanelError, deactivateTrigger} from "../../../../duck/actions/ui/panelActions";
import {submitOrder} from "../../../../duck/actions/service/orderActions";
import {getNextPanel} from "../../manifest";

const currentPanel = RES_PANEL_ORDER;

class OrderMain extends Component {


    trigger = () => {

        this.props.changePanelError('');

        try {
            validateOrder({
                reservationId: this.props.reservationId,
                meals: this.props.clientMeals
            });

        } catch (e) {
            this.props.deactivateTrigger(currentPanel);
            this.props.changePanelError(e.message);
            return;
        }

        this.props.submitOrder({
            reservationId: this.props.reservationId,
            meals: this.props.clientMeals
        });

    };


    extractAllTypes = (meals) => {
        let all = _.map(meals, "type");
        return _.uniq(all);
    };

    changeMealType = (type) => {
        this.setState({mealType: type})
    };

    constructor(props) {
        super(props);
        this.state = {
            mealType: '',
        }
    }


    componentDidMount() {
        this.props.fetchMeals();
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.trigger)
            this.trigger();

        if (nextProps.submitted) {
            this.props.changePanelContent(getNextPanel(currentPanel));
        } else {
            this.props.deactivateTrigger(currentPanel);
            if (this.props.error) {
                this.props.changePanelError(this.props.error);
            }
        }

    }

    render() {
        return (
            <div className={"order-panel"}>
                <Row>
                    <Col lg={2} md={2}>
                        <MealTypes
                            mealTypes={this.extractAllTypes(this.props.meals)}
                            changeMealType={this.changeMealType}
                        />
                    </Col>

                    <Col lg={7} md={7}>
                        <MealsList
                            meals={this.props.meals}
                            type={this.state.mealType}
                        />
                    </Col>

                    <Col lg={3} md={3}>
                        <ClientOrders
                            clientMeals={this.props.clientMeals}
                            price={this.props.price}
                        />
                    </Col>

                    {this.props.error && <Alert>{this.props.error}</Alert>}
                </Row>

            </div>
        );
    }

}

OrderMain.propTypes = {
    clientMeals: PropTypes.array.isRequired,
    price: PropTypes.number.isRequired,
    reservationId: PropTypes.number.isRequired,
    submitted: PropTypes.bool.isRequired,
    trigger: PropTypes.bool.isRequired,

    meals: PropTypes.array,
    error: PropTypes.string,

    fetchMeals: PropTypes.func.isRequired,
    submitOrder: PropTypes.func.isRequired,
    changePanelError: PropTypes.func.isRequired,
    deactivateTrigger: PropTypes.func.isRequired,
    changePanelContent: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    clientMeals: state.order.meals,
    reservationId: state.order.reservationId,
    price: state.order.price,
    meals: state.meal.meals,
    error: state.order.orderError,
    submitted: state.order.sent,
    trigger: state.panel.triggerOrder,
});

export default connect(mapStateToProps, {
    submitOrder,
    fetchMeals,
    changePanelError,
    deactivateTrigger,
    changePanelContent,
})(OrderMain);
