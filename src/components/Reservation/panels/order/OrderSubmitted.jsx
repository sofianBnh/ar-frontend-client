import React, {Component} from 'react';
import {connect} from "react-redux";
import {changeButtonTitle, changePanelContent, resetPanel} from "../../../../duck/actions/ui/panelActions";
import {resetReservation} from "../../../../duck/actions/service/reservationActions";
import {resetOrder} from "../../../../duck/actions/service/orderActions";
import {CURRENCY, RES_PANEL_GREETING} from "../../../../utils/consts";
import PropTypes from 'prop-types';
import {resetRegistration} from "../../../../duck/actions/service/registrationActions";


const text = "Your order has been submitted";

class OrderSubmitted extends Component {

    trigger = () => {
        this.props.resetPanel();
        this.props.resetOrder();
        this.props.resetReservation();
        this.props.resetRegistration();
        this.props.changePanelContent(RES_PANEL_GREETING);
    };

    displayPrice = () => {
        if (this.props.price === this.props.discountedPrice)
            return <span className={"new-price"}>{this.props.price + " " + CURRENCY}</span>;

        return <span>
            <span className={"old-price"}>{this.props.price + " " + CURRENCY}</span>
            <span className={"new-price"}>{this.props.discountedPrice + " " + CURRENCY}</span>
        </span>

    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.trigger)
            this.trigger();
    }

    render() {
        return (
            <div className={"reservation-text"}>
                <div>{text}</div>
                <div className={"price"}>Price: {this.displayPrice()}</div>
            </div>
        );
    };
}

OrderSubmitted.propTypes = {
    price: PropTypes.number.isRequired,
    discountedPrice: PropTypes.number.isRequired,
    trigger: PropTypes.bool.isRequired,
};


const mapStateToProps = state => ({
    trigger: state.panel.triggerOrderSubmitted,
    price: state.order.price,
    discountedPrice: state.order.discountedPrice,
});

export default connect(mapStateToProps, {
    resetPanel,
    resetReservation,
    changePanelContent,
    changeButtonTitle,
    resetRegistration,
    resetOrder,
})(OrderSubmitted);
