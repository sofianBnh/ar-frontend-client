import React from 'react';
import {Badge, Col, ListGroup, ListGroupItem} from "react-bootstrap";
import PropTypes from 'prop-types';
import Row from "react-bootstrap/es/Row";
import {CURRENCY} from "../../../../../utils/consts";
import {removeMealFromOrder} from "../../../../../duck/actions/service/orderActions";
import {connect} from "react-redux";
import _ from "lodash";

const ClientOrders = ({clientMeals, price, removeMealFromOrder}) => {


    function extractOrders(meals) {

        let orders = [];
        let all = _.map(meals, 'name');
        let names = _.uniq(all);
        _.forEach(names, mealName => {
            let count = 0;
            _.forEach(all, name => {
                if (name === mealName)
                    count++;
            });
            orders.push({meal: mealName, count})
        });

        return orders
    }

    const removeMeal = (mealName) => {
        let meal = _.filter(clientMeals, ['name', mealName])[0];
        removeMealFromOrder(meal);
    };


    return (
        <div className={"res-order-client"}>
            <Row>
                <Col lg={12} md={12} sm={6}>
                    <h3> Orders </h3>
                    <div className={"client-orders"}>
                        <ListGroup>
                            {extractOrders(clientMeals).map((order, index) => {
                                return <span key={index}>
                                    <ListGroupItem className={"meal-item"}>
                                    <ClientMeal meal={order.meal} quantity={order.count} removeMeal={removeMeal}/>
                                </ListGroupItem>
                                </span>
                            })}
                        </ListGroup>
                    </div>
                </Col>

                <Col lg={12} md={12} sm={6}>
                    <div className={"orders-price"}>
                        <h3>Price</h3>
                        <p className={"new-price"}>{price + CURRENCY}</p>
                    </div>
                </Col>
            </Row>
        </div>
    );
};


const ClientMeal = ({meal, quantity, removeMeal}) => {
    return (
        <div className={"meal-holder"}
             onClick={() => removeMeal(meal)}>

            <span style={{display: "inline-block", marginRight: "0.5em"}}>{meal}</span><Badge
            pullRight>{quantity}</Badge>
        </div>
    );
};

ClientMeal.propTypes = {
    meal: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
    removeMeal: PropTypes.func.isRequired,
};


ClientOrders.propTypes = {
    clientMeals: PropTypes.array.isRequired,
    price: PropTypes.number.isRequired,

    removeMealFromOrder: PropTypes.func.isRequired,
};

export default connect(null, {removeMealFromOrder})(ClientOrders);
