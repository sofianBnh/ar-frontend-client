import React from 'react';
import {ListGroup, ListGroupItem} from "react-bootstrap";
import PropTypes from 'prop-types';
import {CURRENCY} from "../../../../../utils/consts";
import {addMealToOrder} from "../../../../../duck/actions/service/orderActions";
import {connect} from "react-redux";
import _ from "lodash";

const MealsList = ({meals, addMealToOrder, type}) => {

    const getMealsByType = (type) => {
        if (type !== '')
            return _.filter(meals, ["type", type]);

        return meals;
    };

    const addMeal = (meal) => {
        addMealToOrder(meal);
    };

    return (
        <div className={"res-order-meal-list-container"}>
            <ListGroup className={"res-order-meal-list"}>
                {getMealsByType(type).map((meal, index) => {
                    return <span key={index}>
                        <ListGroupItem className={"meal-item"}>
                        <div className={"meal-holder"}
                             onClick={() => addMeal(meal)}
                        >
                            <Meal meal={meal}/>
                        </div>
                    </ListGroupItem>
                    </span>
                })}
            </ListGroup>
        </div>
    );
};

MealsList.propTypes = {
    meals: PropTypes.array.isRequired,
    addMealToOrder: PropTypes.func.isRequired,
};


const Meal = ({meal}) => {
    return (
        <div className={"res-order-meal-item"}>
            <h4>{meal.name}</h4>
            <p>{meal.description} </p>
            <hr/>
            <p className={"meal-price"}>
                {meal.price + CURRENCY} <span style={{float: "right"}}><i className="fas fa-plus"/></span>
            </p>
        </div>
    );
};


Meal.propTypes = {
    meal: PropTypes.object.isRequired
};

export default connect(null, {addMealToOrder})(MealsList);
