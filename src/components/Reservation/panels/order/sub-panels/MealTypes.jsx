import React from 'react';
import PropTypes from 'prop-types';

const MealTypes = ({mealTypes, changeMealType}) => {

    const mapTypeToIcon = (type) => {
        switch (type) {
            case 'MAIN':
                return 'assets/icons/spaghetti.png';

            case 'STARTER':
                return 'assets/icons/salad.png';

            case 'DESERT':
                return 'assets/icons/ice-cream.png';

            default:
                return 'assets/icons/spaghetti.png';
        }
    };

    return (
        <div className={"meal-types"}>
            {mealTypes.map((mealType, index) => {
                return <div key={index} className={"meal-type-button"} onClick={() => changeMealType(mealType)}>
                    <div className={"meal-types-buttons-container"}>
                        <div className={"meal-types-button"} style={{
                            backgroundImage: "url(" + mapTypeToIcon(mealType) + ")",
                            backgroundSize: "contain",
                        }}/>
                    </div>
                </div>

            })}
        </div>
    );
};

MealTypes.propTypes = {
    mealTypes: PropTypes.array.isRequired,
    changeMealType: PropTypes.func.isRequired,
};

export default MealTypes;
