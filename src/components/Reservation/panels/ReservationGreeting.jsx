import React, {Component} from 'react';
import {RES_PANEL_GREETING} from "../../../utils/consts";
import {getNextPanel} from "../manifest";
import {connect} from "react-redux";
import {changePanelContent, changePanelError} from "../../../duck/actions/ui/panelActions";
import PropTypes from 'prop-types';

const currentPanel = RES_PANEL_GREETING;
const text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. " +
    "Dolor expedita impedit incidunt nemo optio perferendis" +
    " quia, quidem reprehenderit suscipit vitae. Ad animi aut " +
    "in officia ratione reiciendis soluta, tempora velit.\n";

class ReservationGreeting extends Component {


    trigger = () => {
        this.props.changePanelError('');
        this.props.changePanelContent(getNextPanel(currentPanel));
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.trigger && nextProps.trigger !== this.props.trigger) {
            this.trigger();
        }
    }

    render() {
        return (
            <div className={"reservation-text"}>
                {text}
            </div>
        );
    }
}

ReservationGreeting.propTypes = {
    changePanelContent: PropTypes.func.isRequired,
    changePanelError: PropTypes.func.isRequired,
    trigger: PropTypes.bool.isRequired,
};


const mapStateToProps = state => ({
    trigger: state.panel.triggerGreeting,
});

export default connect(mapStateToProps, {changePanelContent, changePanelError})(ReservationGreeting);
