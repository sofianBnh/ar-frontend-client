import React, {Component} from 'react';
import ReservationBar from "./bar/ReservationBar";
import {getPanelByName} from './manifest'
import '../../styles/reservation.css'
import PropTypes from 'prop-types'
import {connect} from "react-redux";
import {Alert} from "react-bootstrap";
import {triggerPanel} from "../../duck/actions/ui/panelActions";


class Reservation extends Component {

    next = () => {
        this.props.triggerPanel(this.props.panel);
    };

    render() {
        return (
            <div className={"reservation"} id={"reservation"}>
                <div className={"container reservation-panel"}>

                    <h2>Reservation</h2>

                    <div className={"reservation-input"}>
                        {getPanelByName(this.props.panel)}
                        {this.props.error && <Alert>{this.props.error}</Alert>}
                    </div>

                    <ReservationBar next={this.next} title={this.props.title}/>

                </div>

            </div>
        );
    }
}

Reservation.propTypes = {
    title: PropTypes.string.isRequired,
    panel: PropTypes.string.isRequired,
    error: PropTypes.string,

    triggerPanel: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    title: state.panel.buttonTitle,
    panel: state.panel.panel,
    error: state.panel.error,
});

export default connect(mapStateToProps, {triggerPanel})(Reservation);
