import React, {Component} from 'react';
import '../../styles/presentation.css'
import {PRESENTATION_COMPONENTS} from "./manifest";

class Presentation extends Component {
    render() {
        return (
            <div>
                {PRESENTATION_COMPONENTS.map((component, index) => {
                    return <div key={index} className={"section"}>{component}</div>
                })}
            </div>
        );
    }
}

Presentation.propTypes = {};

export default Presentation;
