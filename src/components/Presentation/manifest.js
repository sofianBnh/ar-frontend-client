import React from "react";
import Hero from "./sections/Hero";
import About from "./sections/About";
import Services from "./sections/Services";
import Menus from "./sections/Menus";
import Gallery from "./sections/Gallery";
import Discount from "./sections/Discount";

export const LINKS = [
    {title: "About", link: "#about"},
    {title: "Menus", link: "#menus"},
    {title: "Services", link: "#services"},
    {title: "Gallery", link: "#gallery"},
    {title: "Reservation", link: "#reservation"}
];

export const PRESENTATION_COMPONENTS = [
    <Hero/>,
    <About/>,
    <Menus/>,
    <Discount/>,
    <Services/>,
    <Gallery/>,
];