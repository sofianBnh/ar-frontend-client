import React from 'react';
import {Col, Row} from "react-bootstrap";

const About = () => {
    return (
        <div className={"about-holder"} id={"about"}>
            <div className={"container about"}>

                <h2 className={"text-center"}>Who are we?</h2>

                <div className={"about-intro"}>

                    <p className={"about-quote"}>“Perfection is our goal, making extraordinary food is just a
                        hobby”</p>
                    <p className={"about-quote-author"}>Rudy McGooldine</p>
                    <hr/>

                    <p className={"text-center about-long-text"}>Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit. Consectetur
                        dolorem, esse expedita fugit id illum itaque labore laboriosam laudantium maxime minus modi,
                        nisi nobis obcaecati quia quos repellat similique veritatis. Lorem ipsum dolor sit amet,
                        consectetur adipisicing elit. Alias consequuntur cum, dolores illo molestias nostrum praesentium
                        rem repellat, rerum tenetur vero vitae! Dolorum eum hic, laborum nesciunt numquam perspiciatis
                        quidem.
                    </p>

                </div>


                <div className={"about-chef"}/>

                <hr/>


                <div className={"about-text"}>
                    <Row>
                        <Col lg={4} md={4} sm={4}>
                            <div className={"about-sections"}>
                                <h3><span className={"about-icon"}><i className="far fa-star"/></span> Our Chefs</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab autem deleniti
                                    dignissimos dolorum earum eos esse ex exercitationem unde.
                                    dignissimos dolorum earum eos esse ex exercitationem unde.
                                    dignissimos dolorum earum eos esse ex exercitationem unde.
                                </p>
                            </div>
                        </Col>

                        <Col lg={4} md={4} sm={4}>
                            <div className={"about-sections"}>
                                <h3><span className={"about-icon"}><i className="fas fa-utensils"/></span> Our
                                    Restaurants</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab autem deleniti
                                    dignissimos dolorum earum eos esse ex exercitationem unde.
                                    dignissimos dolorum earum eos esse ex exercitationem unde.
                                    dignissimos dolorum earum eos esse ex exercitationem unde.
                                </p>
                            </div>
                        </Col>

                        <Col lg={4} md={4} sm={4}>
                            <div className={"about-sections"}>
                                <h3><span className={"about-icon"}><i className="far fa-gem"/></span> Our Principles
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab autem deleniti
                                    dignissimos dolorum earum eos esse ex exercitationem unde.
                                    dignissimos dolorum earum eos esse ex exercitationem unde.
                                    dignissimos dolorum earum eos esse ex exercitationem unde.
                                </p>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        </div>
    );
};

export default About;
