import React from 'react';
import PropTypes from "prop-types";
import {changeDialog, toggleDialog} from "../../../duck/actions/ui/dialogActions";
import {connect} from "react-redux";
import {REGISTRATION_DIALOG} from "../../../utils/consts";

const Discount = (props) => {

    const displayRegistration = () => {
        props.changeDialog(REGISTRATION_DIALOG);
        props.toggleDialog(true);
    };

    return (
        <div className="discount">
            <div className="container">
                <div>
                    <h2>Register now, and get the best deals !</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci amet dolorem enim in
                        incidunt, modi! Aliquid eum harum illo ipsa ipsam itaque omnis veniam. Corporis facere harum
                        laboriosam suscipit.</p>
                </div>

                <button className={"register-button ghost-button-border-color"}
                        onClick={displayRegistration}>Register
                </button>
            </div>
        </div>
    );
};

Discount.propTypes = {
    toggleDialog: PropTypes.func.isRequired,
    changeDialog: PropTypes.func.isRequired,
};

export default connect(null, {changeDialog, toggleDialog})(Discount);
