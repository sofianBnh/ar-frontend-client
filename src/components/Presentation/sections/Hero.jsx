import React from 'react';
import {Jumbotron} from "react-bootstrap";
import {RESTAURANT_NAME} from "../../../utils/consts";

const content = {
    title: RESTAURANT_NAME,
    button: "Book Right Now !"
};

const Hero = () => {

    return (
        <div className={"hero cover"} id={"hero"}>
            <Jumbotron className={"cover"}>

                <div className="container text-center panel-heading head-text">

                    <h1 className="cover-heading">Welcome to</h1>
                    <h1 className="cover-name">{content.title}</h1>


                    <p className="cover-text">Surprise Yourself<br/>Expand your horizons</p>
                    <a className="ghost-button-border-color about-button" href={"#reservation"}>{content.button}</a>


                </div>

            </Jumbotron>

        </div>
    );
};

export default Hero;
