import React from 'react';
import {Col, Row} from "react-bootstrap";

const content = {
    title: "Menus",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consectetur doloribus " +
    "eius eum eveniet ex expedita explicabo harum iste iure molestiae, nesciunt nihil odio perspiciatis " +
    "porro quia velit veritatis, vitae?\n"

};

const Menus = () => {
    return (
        <div className="menus" id={"menus"}>
            <div className={"container"}>

                <Row className={"big-container"}>

                    <Col lg={6} md={6}>
                        <div className={"menu-big-image"}>
                            <div className={"menu-small-image"}/>
                        </div>
                    </Col>

                    <Col lg={6} md={6}>
                        <div className={'menu-text text-center'}>
                            <h2>{content.title}</h2>
                            <p>{content.description}</p>
                        </div>
                    </Col>


                </Row>

            </div>


        </div>
    );
};

export default Menus;
