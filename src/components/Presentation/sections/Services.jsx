import React from 'react';
import {Col, Row} from "react-bootstrap";
import PropTypes from "prop-types";
import Image from "react-bootstrap/es/Image";

export const SERVICES = [
    {
        name: "Breakfast",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, \n" +
        "                debitis ea eligendi impedit incidunt inventore minima molestiae placeat\n" +
        "                ",
        image: "assets/icons/breakfast.png"
    },
    {
        name: "Lunch",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, \n" +
        "                debitis ea eligendi impedit incidunt inventore minima molestiae placeat\n" +
        "                ",
        image: "assets/icons/fast-food.png"
    },
    {
        name: "Dinner",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, \n" +
        "                debitis ea eligendi impedit incidunt inventore minima molestiae placeat\n" +
        "                ",
        image: "assets/icons/dinner.png"
    }
];

const Services = () => {
    return (
        <div className={"container text-center services"} id={"services"}>

            <h2>Our Services</h2>


            <p className={"services-lead"}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi,
                debitis ea eligendi impedit incidunt inventore minima molestiae placeat
                quas qui quod quos rem unde velit voluptatibus. Adipisci dignissimos
                laudantium ratione qui quod quos rem unde velit voluptatibus. Adipisci dignissimos
                laudantium ratione.</p>


            <hr className={"service-divider"}/>

            <div className={"services-list"}>
                <Row>
                    {SERVICES.map((service, index) => {
                        return <span key={index}><Service
                            name={service.name} description={service.description}
                            image={service.image}/>
                        </span>
                    })}
                </Row>
            </div>

        </div>
    );
};


const Service = ({name, description, image}) => {


    return (
        <Col lg={4} md={4}>

            <div className={"service"}>

                <Image src={image} width={140} height={140}/>
                <h3>{name}</h3>
                <p>{description}</p>
            </div>

        </Col>
    );
};

Service.propTypes = {

    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,

};

export default Services;

