import React from 'react';

const images = [
    {
        link: "assets/img/eaters-collective-132773-unsplash_opt.jpg",
        text: "Pasta con el Sarde"
    }, {
        link: "assets/img/martin-widenka-491478-unsplash_opt.jpg",
        text: "Saumon Fumé"
    }, {
        link: "assets/img/nirzar-pangarkar-31451-unsplash_opt.jpg",
        text: "Quattro Stagioni"
    }, {
        link: "assets/img/sharon-chen-352895-unsplash_opt.jpg",
        text: "Hiyamugi"
    }, {
        link: "assets/img/vitchakorn-koonyosying-516467-unsplash_opt.jpg",
        text: "Pappardelle"
    },
];

const Gallery = () => {
    return (
        <div className={"gallery-holder"} id={"gallery"}>
            <div className="container text-center">
                <h2>Gallery</h2>
                <p className={"gallery-text"}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus
                    ducimus facere molestias qui
                    quod. Alias cumque cupiditate deserunt eaque omnis quas quisquam voluptas! Aspernatur beatae cumque
                    dolor dolorum repellat tempora.</p>

                <hr/>
                <div className={"pictures"}>
                    {images.map((picture, index) => {
                        return <div key={index} className={"gallery-pictures-holder"}>
                            <div className={"gallery-picture"}
                                 style={{backgroundImage: "url(" + picture.link + ")"}}/>

                            <div className={"gallery-picture-overlay"}>
                                <div className={"gallery-picture-text"}>
                                    {picture.text}
                                </div>
                            </div>

                        </div>

                    })}
                </div>
                <hr/>

            </div>

        </div>
    );
};

export default Gallery;
