import React from 'react';
import PropTypes from 'prop-types';

const RegistrationWrapper = (props) => {
    return (
        <div className={"dialog-wrapper"}>
            <h2>User Registration</h2>

            <p>Welcome to the club</p>

            <div>
                {props.children}
            </div>

            <hr/>
        </div>
    );
};


RegistrationWrapper.propTypes = {
    children: PropTypes.object.isRequired
};

export default RegistrationWrapper;
