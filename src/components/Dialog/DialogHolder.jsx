import React, {Component} from 'react';
import {Modal} from "react-bootstrap";
import {LOGIN_DIALOG, REGISTRATION_DIALOG} from "../../utils/consts";
import LoginForm from "../common/forms/LoginForm";
import {connect} from "react-redux";
import {toggleDialog} from "../../duck/actions/ui/dialogActions";
import PropTypes from 'prop-types';
import LoginWrapper from "./LoginWrapper";
import '../../styles/dialog.css';
import RegistrationWrapper from "./RegistrationWrapper";
import RegisterForm from "../common/forms/RegisterForm";

const dialogs = {
    [LOGIN_DIALOG]: {
        title: "LOGIN",
        body: <LoginWrapper>
            <LoginForm/>
        </LoginWrapper>,
    },
    [REGISTRATION_DIALOG]: {
        title: "REGISTRATION",
        body: <RegistrationWrapper>
            <RegisterForm/>
        </RegistrationWrapper>,
    },
};

class DialogHolder extends Component {

    onHide = () => {
        this.props.toggleDialog(false);
    };

    render() {
        return (
            <Modal show={this.props.open} onHide={this.onHide}>

                <div className={"dialog-title"}>
                    <Modal.Header closeButton>
                        <Modal.Title>{dialogs[this.props.type].title}</Modal.Title>
                    </Modal.Header>
                </div>

                <Modal.Body>
                    <div>
                        {dialogs[this.props.type].body}
                    </div>
                </Modal.Body>

            </Modal>

        );
    }
}

const mapStateToProps = state => ({
    open: state.dialog.dialogOpen,
    type: state.dialog.dialogType,
});

DialogHolder.propTypes = {
    open: PropTypes.bool.isRequired,
    type: PropTypes.string.isRequired,
    toggleDialog: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {toggleDialog})(DialogHolder);
