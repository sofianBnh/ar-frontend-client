import React from 'react';
import PropTypes from 'prop-types';
import {RESTAURANT_NAME} from "../../utils/consts";

const LoginWrapper = (props) => {
    return (
        <div className={"dialog-wrapper"}>
            <h2>{RESTAURANT_NAME}</h2>
            <hr/>
            <div>
                {props.children}
            </div>
            <hr/>
        </div>
    );
};


LoginWrapper.propTypes = {
    children: PropTypes.object.isRequired
};

export default LoginWrapper;
