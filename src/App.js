import React, {Component} from 'react';
import './App.css';
import Presentation from "./components/Presentation/Presentation";
import UserReservationPanel from "./components/UserReservations/UserReservationPanel";
import DialogHolder from "./components/Dialog/DialogHolder";
import Header from "./components/Header/Header";
import Reservation from "./components/Reservation/Reservation";
import Footer from "./components/Footer/Footer";

class App extends Component {
    render() {
        return (
            <div>
                <UserReservationPanel/>
                <DialogHolder/>
                <Header/>
                <Presentation/>
                <Reservation/>
                <Footer/>
            </div>
        );
    }
}

export default App;
