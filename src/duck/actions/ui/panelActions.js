import {
    ACTIVATE_TRIGGER_PANEL,
    CHANGE_BUTTON_TITLE_PANEL,
    CHANGE_CONTENT_PANEL,
    CHANGE_ERROR_PANEL,
    DEACTIVATE_TRIGGER_PANEL,
    RESET_PANEL,
} from "../types";
import {
    NEXT,
    ORDER,
    RES_PANEL_GREETING,
    RES_PANEL_ORDER,
    RES_PANEL_ORDER_SUBMITTED,
    RES_PANEL_RESERVATION_SUBMITTED,
    RES_PANEL_RESERVATIONS,
    RES_PANEL_USER,
    RESET,
    SUBMIT
} from "../../../utils/consts";

export const changePanelError = (error) => dispatch => {
    dispatch({
        type: CHANGE_ERROR_PANEL,
        payload: error,
    });
};

export const changeButtonTitle = (title) => dispatch => {
    dispatch({
        type: CHANGE_BUTTON_TITLE_PANEL,
        payload: title,
    })
};

export const changePanelContent = (panelName) => dispatch => {
    dispatch({
        type: CHANGE_CONTENT_PANEL,
        payload: panelName,
    });

    let buttonName;
    switch (panelName) {
        case RES_PANEL_RESERVATIONS:
            buttonName = SUBMIT;
            break;

        case RES_PANEL_RESERVATION_SUBMITTED:
            buttonName = ORDER;
            break;

        case RES_PANEL_ORDER_SUBMITTED:
            buttonName = RESET;
            break;

        default:
            buttonName = NEXT;
            break;
    }

    dispatch({
        type: CHANGE_BUTTON_TITLE_PANEL,
        payload: buttonName
    });
};

export const triggerPanel = (panelName) => dispatch => {
    let triggerName = mapPanelToTrigger(panelName);

    dispatch({
        type: ACTIVATE_TRIGGER_PANEL,
        payload: triggerName,
    });

};

export const deactivateTrigger = (panelName) => dispatch => {
    let triggerName = mapPanelToTrigger(panelName);

    dispatch({
        type: DEACTIVATE_TRIGGER_PANEL,
        payload: triggerName,
    });

};


export const resetPanel = () => dispatch => {

    dispatch({
        type: RESET_PANEL,
    });
};

function mapPanelToTrigger(panelName) {
    let triggerName;
    switch (panelName) {

        case RES_PANEL_GREETING:
            triggerName = 'triggerGreeting';
            break;

        case RES_PANEL_USER:
            triggerName = 'triggerUser';
            break;

        case RES_PANEL_RESERVATIONS:
            triggerName = 'triggerReservation';
            break;

        case RES_PANEL_RESERVATION_SUBMITTED:
            triggerName = 'triggerSubmit';
            break;

        case RES_PANEL_ORDER:
            triggerName = 'triggerOrder';
            break;

        case RES_PANEL_ORDER_SUBMITTED:
            triggerName = 'triggerOrderSubmitted';
            break;

        default:
            triggerName = 'triggerGreeting'

    }
    return triggerName;
}
