import {OPEN_USER_RESERVATION, TOGGLE_USER_RESERVATIONS} from "../types";
import $ from 'jquery';

export const toggleUserReservations = open => dispatch => {

    if (open) {
        $('body').addClass('noscroll');
    } else {
        $('body').removeClass('noscroll');
    }

    dispatch({
        type: TOGGLE_USER_RESERVATIONS,
        payload: open
    });
};

export const openUserReservation = (id) => dispatch => {
    dispatch({
        type: OPEN_USER_RESERVATION,
        payload: id,
    })
};