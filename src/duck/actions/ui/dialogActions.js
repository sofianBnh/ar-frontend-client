import {CHANGE_DIALOG, TOGGLE_DIALOG} from "../types";

export const toggleDialog = open => dispatch => {
    dispatch({
        type: TOGGLE_DIALOG,
        payload: open
    });
};


export const changeDialog = (type) => dispatch => {
    dispatch({
        type: CHANGE_DIALOG,
        payload: type
    });
};