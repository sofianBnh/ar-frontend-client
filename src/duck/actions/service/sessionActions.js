import {
    FETCH_TOKEN,
    FETCH_USER_ID_SESSION,
    FETCH_USER_ID_SESSION_ERROR,
    LOGIN_SESSION,
    LOGIN_SESSION_ERROR,
    LOGOUT_SESSION,
    RESET_ORDER,
    RESET_PANEL,
    RESET_REGISTRATION,
    RESET_RESERVATIONS,
    SET_RESERVATION_OWNER_ID,
    SET_RESERVATION_OWNER_TYPE,
    TOGGLE_DIALOG
} from "../types";
import jwt from "jsonwebtoken";
import axios from 'axios';
import {BEARER, TOKEN_COOKIE_KEY, USER_TYPE} from "../../../utils/consts";
import * as cookies from "react-cookies";
import {getError} from "../../../utils/requests";

const fetchUserIdGeneric = (dispatch) => {


    let request = axios.get('/user-id', {
        headers: {
            Authorization: BEARER + cookies.load(TOKEN_COOKIE_KEY)
        }
    });


    request.then(response => {
        dispatch({
            type: FETCH_USER_ID_SESSION,
            payload: response.data
        });
        dispatch({
            type: SET_RESERVATION_OWNER_ID,
            payload: response.data,
        });

    }).catch(error => {
        dispatch({
            type: FETCH_USER_ID_SESSION_ERROR,
            payload: getError(error),
        })
    });

};


export const login = ({username, password}) => dispatch => {

    clear(dispatch);

    let request = axios.post('/auth', {
        username: username.trim(),
        password: password
    });

    request.then(response => {
        cookies.save(TOKEN_COOKIE_KEY, response.data.token);

        dispatch({
            type: LOGIN_SESSION,
            payload: true
        });

        dispatch({
            type: TOGGLE_DIALOG,
            payload: false,
        });

        dispatch({
            type: SET_RESERVATION_OWNER_TYPE,
            payload: USER_TYPE,
        });

        dispatch({
            type: RESET_REGISTRATION,
        });

        fetchUserIdGeneric(dispatch);


    }).catch(error => {
        dispatch({
            type: LOGIN_SESSION_ERROR,
            payload: getError(error)
        });

    })
};

export const fetchUserId = () => dispatch => {
    fetchUserIdGeneric(dispatch);
};

export const fetchToken = () => dispatch => {

    let token = cookies.load(TOKEN_COOKIE_KEY);

    if (token) {

        if (jwt.decode(token).exp > new Date().getTime()) {
            cookies.remove(TOKEN_COOKIE_KEY);
            clear(dispatch);
            logout();
            return;
        }

        dispatch({
            type: FETCH_TOKEN,
            payload: jwt.decode(token).sub
        });
        
        fetchUserIdGeneric(dispatch);

    }

};

function clear(dispatch) {
    dispatch({type: LOGOUT_SESSION});

    dispatch({type: RESET_RESERVATIONS});

    dispatch({type: RESET_PANEL});

    dispatch({type: RESET_ORDER})
}

export const logout = () => dispatch => {

    cookies.remove(TOKEN_COOKIE_KEY);
    clear(dispatch);
};

