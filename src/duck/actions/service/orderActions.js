import {
    ADD_MEAL_TO_ORDER,
    CHANGE_ERROR_ORDER,
    CHANGE_ERROR_PANEL,
    NEW_ORDER,
    REMOVE_MEAL_FROM_ORDER,
    RESET_ORDER,
    SET_RESERVATION_ORDER
} from "../types";
import * as cookies from "react-cookies";
import {BEARER, TOKEN_COOKIE_KEY} from "../../../utils/consts";
import axios from "axios/index";
import {getError} from "../../../utils/requests";
import _ from 'lodash';

export const addMealToOrder = (meal) => dispatch => {
    dispatch({
        type: ADD_MEAL_TO_ORDER,
        payload: meal
    });
};

export const removeMealFromOrder = (meal) => dispatch => {
    dispatch({
        type: REMOVE_MEAL_FROM_ORDER,
        payload: meal
    });
};

export const resetOrder = () => dispatch => {
    dispatch({type: RESET_ORDER});
};

export const submitOrder = ({reservationId, meals}) => dispatch => {

    let token = cookies.load(TOKEN_COOKIE_KEY);

    if (!token) {
        dispatch({
            type: CHANGE_ERROR_ORDER,
            payload: "Token not found"
        });
        return;
    }

    let mealsIds = _.map(meals, 'id');

    let request = axios.post('/order-service/users', {
        reservationId: reservationId,
        meals: mealsIds,
    }, {
        headers: {
            Authorization: BEARER + token
        }
    });

    request.then(response => {
        dispatch({
            type: NEW_ORDER,
            payload: response.data
        });

    }).catch(error => {

        dispatch({
            type: CHANGE_ERROR_ORDER,
            payload: getError(error),
        });

        dispatch({
            type: CHANGE_ERROR_PANEL,
            payload: getError(error),
        });
    });

};

export const setReservationOrder = (reservationId) => dispatch => {
    dispatch({
        type: SET_RESERVATION_ORDER,
        payload: reservationId,
    });
};