import {SET_RESERVATION_OWNER_TYPE} from "../types";

export const setUserTypeReservationService = (userType) => dispatch => {
    dispatch({
        type: SET_RESERVATION_OWNER_TYPE,
        payload: userType,
    });
};