import {
    RESET_REGISTRATION,
    SET_RESERVATION_OWNER_ID,
    SET_RESERVATION_OWNER_TYPE,
    SIMPLE_USER_REGISTRATION,
    SIMPLE_USER_REGISTRATION_ERROR,
    TOGGLE_DIALOG,
    USER_REGISTRATION,
    USER_REGISTRATION_ERROR
} from "../types";
import axios from 'axios';
import {SIMPLE_USER_TYPE} from "../../../utils/consts";
import {getError} from "../../../utils/requests";

export const registerSimpleUser = ({name, email, phone}) => dispatch => {
    let request = axios.post('/registration-service/simple-user-sign-up', {
        name: name,
        email: email,
        phone: phone
    });

    request.then(response => {

        dispatch({
            type: SIMPLE_USER_REGISTRATION,
            payload: response.data
        });

        dispatch({
            type: SET_RESERVATION_OWNER_TYPE,
            payload: SIMPLE_USER_TYPE,
        });

        dispatch({
            type: SET_RESERVATION_OWNER_ID,
            payload: response.data,
        });

    }).catch(error => {
        dispatch({
            type: SIMPLE_USER_REGISTRATION_ERROR,
            payload: getError(error),
        });
    })
};

export const registerUser = ({name, email, phone, username, password}) => dispatch => {

    let request = axios.post('/registration-service/user-sign-up', {
        name: name,
        email: email,
        phone: phone,
        username: username,
        password: password
    });

    request.then(response => {

        dispatch({
            type: USER_REGISTRATION,
            payload: response.data
        });

        dispatch({
            type: TOGGLE_DIALOG,
            payload: false,
        });

    }).catch(error => {
        dispatch({
            type: USER_REGISTRATION_ERROR,
            payload: getError(error)
        })
    })
};

export const resetRegistration = () => dispatch => {
    dispatch({
        type: RESET_REGISTRATION,
    });
};