import {
    CHANGE_ERROR_PANEL,
    FETCH_RESERVATIONS,
    FETCH_RESERVATIONS_ERROR,
    NEW_RESERVATION,
    NEW_RESERVATION_ERROR,
    RESET_RESERVATION,
    SET_RESERVATION_ORDER,
    UPDATE_USER_RESERVATION,
    UPDATE_USER_RESERVATION_ERROR
} from "../types";

import axios from "axios/index";
import {BEARER, TOKEN_COOKIE_KEY, USER_TYPE} from "../../../utils/consts";
import * as cookies from "react-cookies";
import {getError} from "../../../utils/requests";

const fetchReservationGeneric = (dispatch) => {

    let token = cookies.load(TOKEN_COOKIE_KEY);

    if (!token) {
        dispatch({
            type: FETCH_RESERVATIONS_ERROR,
            payload: "Token not found"
        });
        return;
    }

    let request = axios.get('/reservation-service/user-reservations', {
        headers: {
            Authorization: BEARER + token
        }
    });

    request.then(response => {
        dispatch({
            type: FETCH_RESERVATIONS,
            payload: response.data
        })
    }).catch(error => {
        dispatch({
            type: FETCH_RESERVATIONS_ERROR,
            payload: getError(error)
        })
    })
};

export const fetchReservations = () => dispatch => {
    fetchReservationGeneric(dispatch);
};

export const submitReservation = ({userId, restaurant, places, date, start, end, userType}) => dispatch => {

    let path = "";
    let header = {};

    if (userType === USER_TYPE) {
        path = 'users';

        let token = cookies.load(TOKEN_COOKIE_KEY);

        if (!token) {
            dispatch({
                type: NEW_RESERVATION_ERROR,
                payload: "Token not found"
            });
            return;
        }

        header = {
            Authorization: BEARER + token
        }

    } else
        path = 'simple-users';


    let request = axios.post('/reservation-service/' + path, {
        userId: userId,
        restaurantId: restaurant,
        numberOfPlaces: places,
        date: date,
        startTime: start,
        endTime: end
    }, {
        headers: header
    });


    request.then(response => {
        dispatch({
            type: NEW_RESERVATION,
            payload: response.data
        });

        dispatch({
            type: SET_RESERVATION_ORDER,
            payload: response.data.id,
        });

        fetchReservationGeneric(dispatch);

    }).catch(error => {
        dispatch({
            type: NEW_RESERVATION_ERROR,
            payload: getError(error)
        });


        dispatch({
            type: CHANGE_ERROR_PANEL,
            payload: getError(error),
        });
    })
};

export const deleteReservation = (id) => dispatch => {

    let token = cookies.load(TOKEN_COOKIE_KEY);

    if (!token) {
        dispatch({
            type: UPDATE_USER_RESERVATION_ERROR,
            payload: "Token not found"
        });
        return;
    }

    let request = axios.delete('/reservation-service/' + id, {
        headers: {
            Authorization: BEARER + token
        }
    });

    request.then(response => {
        dispatch({
            type: UPDATE_USER_RESERVATION,
            payload: response.data
        });

        fetchReservationGeneric(dispatch);

    }).catch(error => {

        dispatch({
            type: UPDATE_USER_RESERVATION_ERROR,
            payload: getError(error)
        })
    })

};

export const updateReservation = ({userId, restaurant, places, date, start, end, userType}, old) => dispatch => {
    let token = cookies.load(TOKEN_COOKIE_KEY);

    if (!token) {
        dispatch({
            type: UPDATE_USER_RESERVATION_ERROR,
            payload: "Token not found"
        });
        return;
    }
    let request = axios.put('/reservation-service/' + old.id, {
        userId: userId,
        restaurantId: restaurant,
        numberOfPlaces: places,
        date: date,
        startTime: start,
        endTime: end,
    }, {
        headers: {
            Authorization: BEARER + token
        }
    });

    request.then(response => {
        dispatch({
            type: UPDATE_USER_RESERVATION,
            payload: response.data
        });

        fetchReservationGeneric(dispatch);

    }).catch(error => {

        dispatch({
            type: UPDATE_USER_RESERVATION_ERROR,
            payload: getError(error)
        })
    })

};


export const resetReservation = () => dispatch => {
    dispatch({type: RESET_RESERVATION});
};

