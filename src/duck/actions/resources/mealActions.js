import {CHANGE_ERROR_PANEL, FETCH_MEALS, FETCH_MEALS_ERROR} from "../types";
import axios from "axios/index";
import {getError} from "../../../utils/requests";

export const fetchMeals = () => dispatch => {

    let request = axios.get('/meal-resources/');

    request.then(response => {
        dispatch({
            type: FETCH_MEALS,
            payload: response.data
        })
    }).catch(error => {
        dispatch({
            type: FETCH_MEALS_ERROR,
            payload: getError(error),
        });

        dispatch({
            type: CHANGE_ERROR_PANEL,
            payload: getError(error),
        })
    })

};