import axios from "axios/index";
import {CHANGE_ERROR_PANEL, FETCH_RESERVATIONS_ERROR, FETCH_RESTAURANTS} from "../types";
import {getError} from "../../../utils/requests";

export const fetchRestaurants = () => dispatch => {
    let request = axios.get('/restaurant-resources/restaurants');

    request.then(response => {
        dispatch({
            type: FETCH_RESTAURANTS,
            payload: response.data
        })
    }).catch(error => {
        dispatch({
            type: FETCH_RESERVATIONS_ERROR,
            payload: getError(error),
        });

        dispatch({
            type: CHANGE_ERROR_PANEL,
            payload: getError(error),
        })
    })
};