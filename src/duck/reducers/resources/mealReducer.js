import {FETCH_MEALS, FETCH_MEALS_ERROR} from "../../actions/types";

const initialState = {
    meals: [],

    mealError: '',
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_MEALS:
            return {
                ...state,
                meals: action.payload,
            };

        case FETCH_MEALS_ERROR:
            return {
                ...state,
                mealError: action.payload,
            };

        default:
            return state;

    }
}