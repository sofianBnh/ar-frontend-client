import {FETCH_RESTAURANTS, FETCH_RESTAURANTS_ERROR} from "../../actions/types";


const initialState = {
    restaurants: [],

    restaurantError: '',
};

export default function (state = initialState, action) {
    switch (action.type) {

        case FETCH_RESTAURANTS:
            return {
                ...state,
                restaurants: action.payload,
                restaurantError: '',
            };

        case FETCH_RESTAURANTS_ERROR:
            return {
                ...state,
                restaurantError: action.payload,
            };


        default:
            return state;

    }

}