import {
    RESET_REGISTRATION,
    SIMPLE_USER_REGISTRATION,
    SIMPLE_USER_REGISTRATION_ERROR,
    USER_REGISTRATION,
    USER_REGISTRATION_ERROR
} from "../../actions/types";

const initialState = {
    user: {},
    userId: '',
    simpleUserId: '',

    simpleUserRegistered: false,
    userRegistrationError: '',
    simpleUserRegistrationError: '',
};


export default function (state = initialState, action) {
    switch (action.type) {

        case SIMPLE_USER_REGISTRATION:
            return {
                ...state,
                simpleUserId: action.payload,
                simpleUserRegistered: true,
                simpleUserRegistrationError: '',
            };

        case SIMPLE_USER_REGISTRATION_ERROR:
            return {
                ...state,
                simpleUserRegistrationError: action.payload,
            };


        case USER_REGISTRATION:
            return {
                ...state,
                userId: action.payload,
                userRegistrationError: '',
            };

        case USER_REGISTRATION_ERROR:
            return {
                ...state,
                userRegistrationError: action.payload,
            };

        case RESET_REGISTRATION:
            return {
                ...state,
                user: {},
                userId: '',
                simpleUserId: '',

                simpleUserRegistered: false,
                userRegistrationError: '',
                simpleUserRegistrationError: '',
            };


        default:
            return state;

    }

}