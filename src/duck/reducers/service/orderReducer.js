import {
    ADD_MEAL_TO_ORDER,
    NEW_ORDER,
    REMOVE_MEAL_FROM_ORDER,
    RESET_ORDER,
    SET_RESERVATION_ORDER
} from "../../actions/types";

const initialState = {
    reservationId: '',
    meals: [],

    price: 0,
    discountedPrice: 0,
    sent: false,
    orderError: '',
};

export default function (state = initialState, action) {
    switch (action.type) {

        case ADD_MEAL_TO_ORDER:
            let arrayWithMeal = state.meals;
            arrayWithMeal.push(action.payload);

            return {
                ...state,
                meals: arrayWithMeal,
                price: state.price + action.payload.price,
            };

        case REMOVE_MEAL_FROM_ORDER:
            let newArray = state.meals;

            let index = newArray.indexOf(action.payload);
            if (index > -1) {
                newArray.splice(index, 1);
            }

            return {
                ...state,
                meals: newArray,
                price: state.price - action.payload.price,
            };

        case SET_RESERVATION_ORDER:
            return {
                ...state,
                reservationId: action.payload,
            };

        case NEW_ORDER:
            return {
                ...state,
                sent: true,
                discountedPrice: action.payload,
            };


        case RESET_ORDER:
            return {
                ...state,
                reservationId: '',
                meals: [],

                price: 0,
                sent: false,
                orderError: '',
            };

        default:
            return state;

    }
}