import {
    UPDATE_USER_RESERVATION,
    UPDATE_USER_RESERVATION_ERROR,
    FETCH_RESERVATIONS,
    FETCH_RESERVATIONS_ERROR,
    NEW_RESERVATION,
    NEW_RESERVATION_ERROR,
    RESET_RESERVATION,
    RESET_RESERVATIONS
} from "../../actions/types";


const initialState = {

    reservations: [],

    reservation: {},

    submitted: false,
    reservationsError: '',
    reservationError: '',
    reservationDeleteError: '',

};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_RESERVATIONS:
            return {
                ...state,
                reservations: action.payload,
                reservationsError: '',
            };

        case FETCH_RESERVATIONS_ERROR:
            return {
                ...state,
                reservationsError: action.payload
            };


        case NEW_RESERVATION:
            return {
                ...state,
                reservation: action.payload,
                reservationError: '',
                submitted: true,
            };

        case NEW_RESERVATION_ERROR:
            return {
                ...state,
                reservationsError: action.payload
            };


        case UPDATE_USER_RESERVATION:
            return {
                ...state,
                reservations: [],
                reservationDeleteError: '',
            };


        case UPDATE_USER_RESERVATION_ERROR:
            return {
                ...state,
                reservationDeleteError: action.payload,
            };

        case RESET_RESERVATION:
            return {
                ...state,
                submitted: false,
                reservationsError: '',
                reservationError: '',
                reservationDeleteError: '',
            };

        case RESET_RESERVATIONS:
            return {
                ...state,
                ...initialState,
            };

        default:
            return state;

    }

}