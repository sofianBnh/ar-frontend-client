import {
    FETCH_TOKEN,
    FETCH_USER_ID_SESSION,
    FETCH_USER_ID_SESSION_ERROR,
    LOGIN_SESSION,
    LOGIN_SESSION_ERROR,
    LOGOUT_SESSION
} from "../../actions/types";

const initialState = {
    username: '',
    loggedUserId: '',
    logged: false,
    fetchedId: false,

    loginError: '',
    loggedUserIdError: '',
};


export default function (state = initialState, action) {
    switch (action.type) {

        case LOGIN_SESSION:
            return {
                ...state,
                logged: action.payload,
                loginError: ''
            };

        case LOGIN_SESSION_ERROR:
            return {
                ...state,
                loginError: action.payload
            };


        case FETCH_USER_ID_SESSION:
            return {
                ...state,
                loggedUserId: action.payload,
                fetchedId: true,
                loggedUserIdError: '',
            };

        case FETCH_USER_ID_SESSION_ERROR:
            return {
                ...state,
                userIdError: action.payload
            };


        case LOGOUT_SESSION:
            return {
                ...state,
                ...initialState
            };


        case FETCH_TOKEN:
            return {
                ...state,
                logged: true,
                username: action.payload,
                loggedUserId: '',
                loginError: '',
                userIdError: '',
            };

        default:
            return state;

    }

}