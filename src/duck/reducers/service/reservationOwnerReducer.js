import {SET_RESERVATION_OWNER_ID, SET_RESERVATION_OWNER_TYPE} from "../../actions/types";
import {USER_TYPE} from "../../../utils/consts";

const initialState = {
    userId: '',
    userType: USER_TYPE,
};

export default function (state = initialState, action) {
    switch (action.type) {

        case SET_RESERVATION_OWNER_ID:
            return {
                ...state,
                userId: action.payload,
            };

        case SET_RESERVATION_OWNER_TYPE:
            return {
                ...state,
                userType: action.payload,
            };

        default:
            return state;

    }
}