import {OPEN_USER_RESERVATION, TOGGLE_USER_RESERVATIONS} from "../../actions/types";

const initialState = {
    userReservationsOpen: false,
    openReservation: '',
};


export default function (state = initialState, action) {
    switch (action.type) {

        case OPEN_USER_RESERVATION:
            return {
                ...state,
                openReservation: action.payload,
            };

        case TOGGLE_USER_RESERVATIONS:
            return {
                ...state,
                userReservationsOpen: action.payload
            };

        default:
            return state;

    }
}