import {CHANGE_DIALOG, TOGGLE_DIALOG} from "../../actions/types";
import {LOGIN_DIALOG} from "../../../utils/consts";

const initialState = {
    dialogOpen: false,
    dialogType: LOGIN_DIALOG

};

export default function (state = initialState, action) {
    switch (action.type) {

        case TOGGLE_DIALOG:
            return {
                ...state,
                dialogOpen: action.payload
            };

        case CHANGE_DIALOG:
            return {
                ...state,
                dialogType: action.payload
            };

        default:
            return state;

    }
}