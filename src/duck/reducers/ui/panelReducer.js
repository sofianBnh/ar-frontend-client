import {
    ACTIVATE_TRIGGER_PANEL,
    CHANGE_BUTTON_TITLE_PANEL,
    CHANGE_CONTENT_PANEL,
    CHANGE_ERROR_PANEL,
    DEACTIVATE_TRIGGER_PANEL,
    RESET_PANEL,
} from "../../actions/types";
import {RES_PANEL_GREETING} from "../../../utils/consts";

const initialState = {
    buttonTitle: 'NEXT',

    panel: RES_PANEL_GREETING,
    error: '',

    triggerGreeting: false,
    triggerUser: false,
    triggerReservation: false,
    triggerSubmit: false,
    triggerOrder: false,
    triggerOrderSubmitted: false,
};


export default function (state = initialState, action) {
    switch (action.type) {

        case CHANGE_ERROR_PANEL:
            return {
                ...state,
                error: action.payload,
            };

        case CHANGE_CONTENT_PANEL:
            return {
                ...state,
                panel: action.payload,
            };


        case CHANGE_BUTTON_TITLE_PANEL:
            return {
                ...state,
                buttonTitle: action.payload
            };


        case ACTIVATE_TRIGGER_PANEL:
            return {
                ...state,
                [action.payload]: true,
            };

        case DEACTIVATE_TRIGGER_PANEL:
            return {
                ...state,
                [action.payload]: false,
            };

        case RESET_PANEL:
            return {
                ...state,
                ...initialState,
            };

        default:
            return state;
    }
}