import {combineReducers} from "redux";
import registrationReducer from "./service/registrationReducer";
import reservationReducer from "./service/reservationReducer";
import restaurantReducer from "./resources/restaurantReducer";
import sessionReducer from "./service/sessionReducer";
import dialogReducer from "./ui/dialogReducer";
import userReservationsReducer from "./ui/userReservationsReducer";
import panelReducer from "./ui/panelReducer";
import reservationOwnerReducer from "./service/reservationOwnerReducer";
import mealReducer from "./resources/mealReducer";
import orderReducer from "./service/orderReducer";


export default combineReducers({
    reservationOwner: reservationOwnerReducer,
    userReservations: userReservationsReducer,
    registration: registrationReducer,
    reservation: reservationReducer,
    restaurant: restaurantReducer,
    session: sessionReducer,
    dialog: dialogReducer,
    order: orderReducer,
    panel: panelReducer,
    meal: mealReducer,
})