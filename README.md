# Restaurant Client SPA

## Overview 

This project is the frontend Single Page Application for the [restaurant project](https://bitbucket.org/sofianBnh/ar-backend-api/src/master/). Build with React and Redux, the application serves both as a portfolio for the website and as a platform for orders and reservations. 


## Project Structure

The project is structured as follows:

```
.
├── package.json                  # Dependencies of the application
├── public                        # public resources
│   ├── favicon.ico               # Icons of the application
│   ├── index.html                # Html Entry Point of the application
│   └── manifest.json             # SPA configuration
├── README.md                     # This documents
├── src                           # Source Code
│   ├── App.css                   # CSS for the Entry Point
│   ├── App.js                    # Entry Point of the Application
│   ├── assets                    # Static Assets
│   ├── components                # React components 
│   ├── duck                      # Redux actionTypes, actions and reducer
│   ├── index.js                  # Index of the front end
│   ├── registerServiceWorker.js  # Web API for caching assets
│   ├── styles                    # CSS for the components
│   └── utils                     # General Utilities
└── yarn.lock                     # Yarn module manager lock
```

## Start Project

For starting the Front in development mode the following steps are followed:

Installing node:
```bash
sudo apt-get install curl python-software-properties
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install nodejs
```

Installing yarn:
```bash
sudo npm install -g yarn
```

Downloading the dependencies:

```bash
cd ar-frontend-client/
yarn
```

In case of a problem do the following:

```bash
rm -rf node_modules
sudo apt remove --purge nodejs
sudo apt install nodejs
yarn
```

Starting the front end:

```bash
yarn start
```

Building:

```bash
yarn build
```